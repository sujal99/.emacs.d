;;; done.el --- This file contain implementation of greeting message -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;; Copyright (C) 2012  Yuriy V. Pitomets <pitometsu@gmail.com>
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defface hello-face0
  '((t (:height 2.0)))
  "Hello message face0")
(defface hello-face1
  '((t (:foreground "DarkOrange" :weight bold :inherit hello-face0)))
  "Hello message face1")
(defface hello-face2
  '((t (:foreground "LimeGreen" :weight bold :inherit hello-face0)))
  "Hello message face2")
(defface hello-face3
  '((t (:foreground "DarkCyan" :weight bold :inherit hello-face0)))
  "Hello message face3")

(defvar welcome-message nil
  "Default welcome message, that appears after loading initialization file.")
(setq welcome-message
      (format (propertize "All done, %s. %s Nya-nya-nya %s" 'face 'hello-face0)
              (propertize user-login-name 'face 'hello-face1)
              (propertize "^_^"           'face 'hello-face2)
              (propertize ":3"            'face 'hello-face3)))

(message "%s" welcome-message)

(cond
 ((featurep 'notify)
  (notify "Welcome!" welcome-message))
 ((featurep 'alert)
  (alert welcome-message :title "Welcome!" :style 'growl)))
