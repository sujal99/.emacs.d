;;; hacks.el --- This file contain some hacks that still haven't become a plugins -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;; Copyright (C) 2012  Yuriy V. Pitomets <pitometsu@gmail.com>
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; All “yes” or “no” questions are aliased to “y” or “n”
(fset 'yes-or-no-p 'y-or-n-p)

;; Should be able to kill processes without asking which is achieved in the second expression
(setq kill-buffer-query-functions
      (remq 'process-kill-buffer-query-function
            kill-buffer-query-functions))

;; nice little alternative visual bell; Miles Bader <miles /at/ gnu.org>
(defcustom echo-area-bell-string "♪" ;"*DING* " ;"♪"
  "Message displayed in mode-line by `echo-area-bell' function."
  :group 'user)
(defcustom echo-area-bell-delay 0.1
  "Number of seconds `echo-area-bell' displays its message."
  :group 'user)
;; internal variables
(defvar echo-area-bell-cached-string nil)
(defvar echo-area-bell-propertized-string nil)
(defun echo-area-bell ()
  "Briefly display a highlighted message in the echo-area.
     The string displayed is the value of `echo-area-bell-string',
     with a red background; the background highlighting extends to the
     right margin.  The string is displayed for `echo-area-bell-delay'
     seconds.
     This function is intended to be used as a value of `ring-bell-function'."
  (unless (equal echo-area-bell-string echo-area-bell-cached-string)
    (setq echo-area-bell-propertized-string
          (propertize
           (concat
            (propertize
             "x"
             'display
             `(space :align-to (- right ,(+ 2 (length echo-area-bell-string)))))
            echo-area-bell-string)
           'face '(:background "#e8e8e8")))
    (setq echo-area-bell-cached-string echo-area-bell-string))
  (message echo-area-bell-propertized-string)
  (sit-for echo-area-bell-delay)
  (message ""))
(setq-default ring-bell-function 'echo-area-bell)

;; use the predefined fontset and custom for cyrillic: пример кириллицы
(set-fontset-font t
                  'unicode
                  '("DejaVu Sans" . "iso10646-1"))
(set-fontset-font t
                  'korean-ksc5601
                  '("Arial Unicode MS" . "iso10646-1"))
(set-fontset-font t
                  'chinese-big5-1
                  '("Arial Unicode MS" . "iso10646-1"))
(set-fontset-font t
                  'chinese-big5-2
                  '("Arial Unicode MS" . "iso10646-1"))
(set-fontset-font t
                  'chinese-gb2312
                  '("Arial Unicode MS" . "iso10646-1"))
(set-fontset-font t
                  'japanese-jisx0208
                  '("MigMix 1M" . "iso10646-1"))
(set-fontset-font t
                  'japanese-jisx0212
                  '("MigMix 1M" . "iso10646-1"))
(set-fontset-font t
                  'mule-unicode-0100-24ff
                  (font-spec :inherit nil))
(set-fontset-font t
                  'cyrillic-iso8859-5
                  (font-spec :inherit nil))
(set-fontset-font t
                  'greek-iso8859-7
                  (font-spec :inherit nil))
(set-fontset-font t
                  'ascii
                  (font-spec :inherit nil))
(set-fontset-font t
                  'symbol
                  (font-spec :inherit nil))

;; чтобы не πодтверждать закрытие буфера
(defun kill-current-buffer ()
  (interactive)
  (kill-buffer (current-buffer)))

;; imenu for emacs-lisp customizations
(defun imenu-elisp-sections ()
  (setq imenu-prev-index-position-function nil)
  (add-to-list 'imenu-generic-expression '("Sections" "^\n+;+ \\(.+\\)$" 1) t))

;; duplication line
(defun duplicate-line-or-region-above (&optional reverse)
  "Duplicate current line or region above.
By default, duplicate current line above.
If mark is activate, duplicate region lines above.
Default duplicate above, unless option REVERSE is non-nil."
  (interactive)
  (let ((origianl-column (current-column))
        duplicate-content)
    (if mark-active
        ;; If mark active.
        (let ((region-start-pos (region-beginning))
              (region-end-pos (region-end)))
          ;; Set duplicate start line position.
          (setq region-start-pos (progn
                                   (goto-char region-start-pos)
                                   (line-beginning-position)))
          ;; Set duplicate end line position.
          (setq region-end-pos (progn
                                 (goto-char region-end-pos)
                                 (line-end-position)))
          ;; Get duplicate content.
          (setq duplicate-content (buffer-substring region-start-pos region-end-pos))
          (if reverse
              ;; Go to next line after duplicate end position.
              (progn
                (goto-char region-end-pos)
                (forward-line +1))
            ;; Otherwise go to duplicate start position.
            (goto-char region-start-pos)))
      ;; Otherwise set duplicate content equal current line.
      (setq duplicate-content (buffer-substring
                               (line-beginning-position)
                               (line-end-position)))
      ;; Just move next line when `reverse' is non-nil.
      (and reverse (forward-line 1))
      ;; Move to beginning of line.
      (beginning-of-line))
    ;; Open one line.
    (open-line 1)
    ;; Insert duplicate content and revert column.
    (insert duplicate-content)
    (move-to-column origianl-column t)))

(defun duplicate-line-or-region-below ()
  "Duplicate current line or region below.
By default, duplicate current line below.
If mark is activate, duplicate region lines below."
  (interactive)
  (duplicate-line-or-region-above t))

(defun duplicate-line-above-comment (&optional reverse)
  "Duplicate current line above, and comment current line."
  (interactive)
  (if reverse
      (duplicate-line-or-region-below)
    (duplicate-line-or-region-above))
  (save-excursion
    (if reverse
        (forward-line -1)
      (forward-line +1))
    (comment-or-uncomment-region+)))

(defun duplicate-line-below-comment ()
  "Duplicate current line below, and comment current line."
  (interactive)
  (duplicate-line-above-comment t))

;; one-key macros record
(defun toggle-kbd-macro-recording-on ()
  "One-key keyboard macros: turn recording on."
  (interactive)
  (define-key
    global-map
    (this-command-keys)
    'toggle-kbd-macro-recording-off)
  (start-kbd-macro nil))

(defun toggle-kbd-macro-recording-off ()
  "One-key keyboard macros: turn recording off."
  (interactive)
  (define-key
    global-map
    (this-command-keys)
    'toggle-kbd-macro-recording-on)
  (end-kbd-macro))

;; scroll text by one line
(defun scroll-one-down ()
  (interactive)
  (scroll-down 1)
  (previous-line))

(defun scroll-one-up ()
  (interactive)
  (scroll-up 1)
  (next-line))

(defun line-up ()
  (interactive)
  (scroll-up 1))

(defun line-down ()
  (interactive)
  (scroll-down 1))

;; kill whole line
(defun kill-start-of-line ()
  "kill from point to start of line."
  (interactive)
  (kill-line 0))

;; remove indentation
(defun remove-indentation ()
  "Remove indentation of current line."
  (interactive)
  (save-excursion
	(back-to-indentation)
	(kill-start-of-line)))

;; eval region
(defun eval-region-and-unmark (START END &optional PRINTFLAG READ-FUNCTION)
  "Interactive execute the region as Lisp code and unmark this region.
See `eval-region'."
  (interactive "r")
  (eval-region START END PRINTFLAG READ-FUNCTION)
  (push-mark))


;; global text scale mode
(define-globalized-minor-mode global-text-scale-mode
  text-scale-mode
  (lambda () (text-scale-mode 1)))

(defun global-text-scale-adjust (inc) (interactive)
  (text-scale-set 1)
  (kill-local-variable 'text-scale-mode-amount)
  (setq-default text-scale-mode-amount (+ text-scale-mode-amount inc))
  (global-text-scale-mode 1))



;; Dim parentheses
(defface paren-face
  '((t
     (:foreground "grey35" :weight bold)))
  "Face used to dim parentheses."
  ;;  :group faces
  )

(defface lisp-paren-face
  '((t
     (:foreground "grey55" :weight bold)))
  "Face used to dim parentheses."
  ;;  :group faces
  )


(defface ruby-paren-face
  '((t
     (:foreground "DimGray" :weight bold)))
  "Face used to dim parentheses."
  ;;  :group faces
  )

(defface eol-face
  '((t
     (:foreground "#CBCBCB" :weight bold)))
  "Face used to dim EOL symbol. See `fill-column-indicator'."
  ;;  :group faces
  )

(defun dim-paren-lisp (mode)
  "Dim parentheses in lisp-style code."
  (font-lock-add-keywords mode
                          '(("(\\|)" . 'lisp-paren-face))))

(defun dim-paren-c (mode)
  "Dim parentheses in c-style code."
  (font-lock-add-keywords mode
                          '(("(\\|)\\|\\[\\|\\]\\|{\\|}" . 'paren-face))))


(defun dim-paren-ruby (mode)
  "Dim parentheses in ruby code."
  (font-lock-add-keywords mode
                          '(("(\\|)\\|\\[\\|\\]\\|{\\|}\\|,\\|\\.\\|=>" . 'ruby-paren-face))))

(defun dim-paren-json (mode)
  "Dim punctuation in json code."
  (font-lock-add-keywords mode
                          '(("{\\|}\\|\\,\\|\\.\\|\\:\\|\\;\\|=" . 'paren-face))))

(defun font-lock-json (mode)
  "Dim punctuation in json code."
  (font-lock-add-keywords mode
                          '(("\\(\\,\\|^\\|[ \n\t]\\|{\\)\\(\"[^\" \n\t]*\"\\)[ \n\t]*:\\|=" (2 'font-lock-builtin-face prepend))) t))

(defun dim-eol (mode)
  "Dim EOL symbol in your code. See `fill-column-indicator'."
  (font-lock-add-keywords mode
                          '(("¬" . 'eol-face))))

(dim-paren-lisp 'emacs-lisp-mode)
(dim-paren-c    'objc-mode)
(dim-paren-c    'sclang-mode)
(dim-paren-c    'haskell-mode)
(dim-paren-ruby 'ruby-mode)
(dim-paren-ruby 'enh-ruby-mode)
(dim-paren-json 'js-mode)
(dim-paren-json 'restclient-mode)
(font-lock-json 'js-mode)
(font-lock-json 'restclient-mode)


;; marks
(defun push-mark-no-activate ()
  "Pushes `point' to `mark-ring' and does not activate the region
Equivalent to \\[set-mark-command] when \\[transient-mark-mode] is disabled"
  (interactive)
  (push-mark (point) t nil)
  (message "Pushed mark to ring"))

(defun jump-to-mark ()
  "Jumps to the local mark, respecting the `mark-ring' order.
This is the same as using \\[set-mark-command] with the prefix argument."
  (interactive)
  (set-mark-command 1))

(defun exchange-point-and-mark-no-activate ()
  "Identical to \\[exchange-point-and-mark] but will not activate the region."
  (interactive)
  (exchange-point-and-mark)
  (deactivate-mark nil))

;; insert postfix whitespace
(defun insert-postfix-whitespace ()
  "Just insert SPC symbol next to point."
  (interactive)
  (save-excursion
	(insert ?\s)
	(backward-char)))

;;show buffer file name
(defun expand-buffer-file-name ()
  "Show the full path to the current file in the minibuffer."
  (interactive)
  (let ((file-name (buffer-file-name)))
    (if file-name
        (progn
          (message file-name)
          (kill-new file-name))
      (error "Buffer not visiting a file"))))

;; search at both stackoverflow and google code
(defun w3mext-hacker-search ()
  "search word under cursor in google code search and stackoverflow.com"
  (interactive)
  (require 'w3m)
  (let ((keyword (w3m-url-encode-string (thing-at-point 'symbol))))
    (browse-url-generic (concat "http://code.google.com/codesearch?q=" keyword))
    (browse-url-generic (concat "http://www.google.com.au/search?hl=en&q=" keyword "+site:stackoverflow.com"))))

;; -----------------------------------------------------------------------------
;;;  browse-apropos-url
;;   from http://www.emacswiki.org/emacs/BrowseAproposURL

(setq apropos-url-alist
      '(("^gw?:? +\\(.*\\)" . ;; Google Web
         "http://www.google.com/search?q=\\1")

        ("^g!:? +\\(.*\\)" . ;; Google Lucky
         "http://www.google.com/search?btnI=I%27m+Feeling+Lucky&q=\\1")

        ("^gl:? +\\(.*\\)" .  ;; Google Linux
         "http://www.google.com/linux?q=\\1")

        ("^gi:? +\\(.*\\)" . ;; Google Images
         "http://images.google.com/images?sa=N&tab=wi&q=\\1")

        ("^gg:? +\\(.*\\)" . ;; Google Groups
         "http://groups.google.com/groups?q=\\1")

        ("^gd:? +\\(.*\\)" . ;; Google Directory
         "http://www.google.com/search?&sa=N&cat=gwd/Top&tab=gd&q=\\1")

        ("^gn:? +\\(.*\\)" . ;; Google News
         "http://news.google.com/news?sa=N&tab=dn&q=\\1")

        ;; Google Translate URL
        ("^gt:? +\\(\\w+\\)|? *\\(\\w+\\) +\\(\\w+://.*\\)" .
         "http://translate.google.com/translate?langpair=\\1|\\2&u=\\3")

        ("^gt:? +\\(\\w+\\)|? *\\(\\w+\\) +\\(.*\\)" . ;; Google Translate Text
         "http://translate.google.com/translate_t?langpair=\\1|\\2&text=\\3")

        ("^/\\.$" . ;; Slashdot
         "http://www.slashdot.org")

        ("^/\\.:? +\\(.*\\)" . ;; Slashdot search
         "http://www.osdn.com/osdnsearch.pl?site=Slashdot&query=\\1")

        ("^fm$" . ;; Freshmeat
         "http://www.freshmeat.net")

        ("^ewiki:? +\\(.*\\)" . ;; Emacs Wiki Search
         "http://www.emacswiki.org/cgi-bin/wiki?search=\\1")

        ("^ewiki$" . ;; Emacs Wiki
         "http://www.emacswiki.org")))

(defun browse-apropos-url (text &optional new-window)
  (interactive (browse-url-interactive-arg "Location: "))
  (let ((text (replace-regexp-in-string
               "^ *\\| *$" ""
               (replace-regexp-in-string "[ \t\n]+" " " text))))
    (let ((url (assoc-default
                text apropos-url-alist
                '(lambda (a b) (let () (setq __braplast a) (string-match a b)))
                text)))
      (browse-url (replace-regexp-in-string __braplast url text) new-window))))

;; -----------------------------------------------------------------------------
;;;   Here is some wrapper code to facilitate access to the apropos browse using
;;    a region/word at point or a prompt.

(defun rgr/region-then-thing-at-point()
  "Function to return the currently selected region.
If no region is selected then return the word at the cursor."
  (if mark-active
      (buffer-substring-no-properties (region-beginning)(region-end))
    (progn
      (let ((word (current-word)))
        (if (zerop (length word))
            (setq word "default"))
        word))))

(defun rgr/google (term)
  "Call google search for the specified term.
Do not call if string is zero length."
  (if (not (zerop (length term)))
      (browse-apropos-url (concat "gw: " term))
    (browse-apropos-url  "http://www.google.com ")))

(defun rgr/google-search-auto ()
  (interactive)
  (let ((word (rgr/region-then-thing-at-point)))
    (rgr/google word)))

(defun rgr/google-search-prompt(term)
  "Prompt user to query google search for term.
Term is word at point or the selcted region"
  (interactive (list (unless (eq current-prefix-arg 0)
                       (read-string "Google for word : "
                                    (rgr/region-then-thing-at-point)))))
  (rgr/google term))

;; command for cat
(defun cat-command ()
  "A command for cats."
  (interactive)
  (require 'animate)
  (let ((mouse "
___00
~~/____'>
\"  \"
")
        (h-pos (floor (/ (window-height) 2)))
        (contents (buffer-string))
        (mouse-buffer (generate-new-buffer "*mouse*")))
    (save-excursion
      (switch-to-buffer mouse-buffer)
      (insert contents)
      (setq truncate-lines t)
      (animate-string mouse h-pos 0)
      (dotimes (_ (window-width))
        (sit-for 0.01)
        (dotimes (n 3)
          (goto-line (+ h-pos n 2))
          (move-to-column 0)
          (insert " "))))
    (kill-buffer mouse-buffer)))

;; use Hyper as Super and AltGr as Meta on Linux
(setq x-alt-keysym 'meta)
(setq x-hyper-keysym 'super)
;; setup modifers for Mac
(setq mac-option-modifier 'meta)
(setq mac-command-modifier 'super)

;; check auto-mode-alist for just created buffers also

;; hack that enable major mode for newly created buffer according to `auto-mode-alist'
(defadvice set-buffer-major-mode (after new-buffer-auto-mode activate compile)
  "Select major mode for newly created buffer.

Compare the `buffer-name' the entries in `auto-mode-alist'."
  (with-current-buffer (ad-get-arg 0)
    (if (and (buffer-name) (not buffer-file-name))
        (let ((name (buffer-name)))
          ;; Remove backup-suffixes from file name.
          (setq name (file-name-sans-versions name))
          ;; Do not handle service buffers
          (while (and name (not (string-match "^\\*.+\\*$" name)))
            ;; Find first matching alist entry.
            (let ((mode (if (memq system-type '(windows-nt cygwin))
                            ;; System is case-insensitive.
                            (let ((case-fold-search t))
                              (assoc-default name auto-mode-alist
                                             'string-match))
                          ;; System is case-sensitive.
                          (or
                           ;; First match case-sensitively.
                           (let ((case-fold-search nil))
                             (assoc-default name auto-mode-alist
                                            'string-match))
                           ;; Fallback to case-insensitive match.
                           (and auto-mode-case-fold
                                (let ((case-fold-search t))
                                  (assoc-default name auto-mode-alist
                                                 'string-match)))))))
              (if (and mode
                       (consp mode)
                       (cadr mode))
                  (setq mode (car mode)
                        name (substring name 0 (match-beginning 0)))
                (setq name nil))
              (when mode
                (set-auto-mode-0 mode t))))))))

;; Emacs equivalent to VIM's `%`
(defun insert-filename-or-buffername (&optional arg)
  "If the buffer has a file, insert the base name of that file.
  Otherwise insert the buffer name.  With prefix argument, insert the full file name."
  (interactive "P")
  (let* ((buffer (window-buffer (minibuffer-selected-window)))
         (file-path-maybe (buffer-file-name buffer)))
    (insert (if file-path-maybe
                (if arg
                    file-path-maybe
                  (file-name-nondirectory file-path-maybe))
              (buffer-name buffer)))))

;; alias for usage in eshell
(defalias 'w3m 'w3m-find-file)
(defalias 'open 'find-file)
(defalias 'openo 'find-file-other-window)

;; This makes Eshell’s ‘ls’ file names RET-able. Yay!
(eval-after-load "em-ls"
  '(progn
     (defun ted-eshell-ls-find-file-at-point (point)
       "RET on Eshell's `ls' output to open files."
       (interactive "d")
       (find-file (buffer-substring-no-properties
                   (previous-single-property-change point 'help-echo)
                   (next-single-property-change point 'help-echo))))

     (defun pat-eshell-ls-find-file-at-mouse-click (event)
       "Middle click on Eshell's `ls' output to open files.
 From Patrick Anderson via the wiki."
       (interactive "e")
       (ted-eshell-ls-find-file-at-point (posn-point (event-end event))))

     (let ((map (make-sparse-keymap)))
       (define-key map (kbd "RET")      'ted-eshell-ls-find-file-at-point)
       (define-key map (kbd "<return>") 'ted-eshell-ls-find-file-at-point)
       (define-key map (kbd "<mouse-2>") 'pat-eshell-ls-find-file-at-mouse-click)
       (defvar ted-eshell-ls-keymap map))

     (defadvice eshell-ls-decorated-name (after ted-electrify-ls activate)
       "Eshell's `ls' now lets you click or RET on file names to open them."
       (add-text-properties 0 (length ad-return-value)
                            (list 'help-echo "RET, mouse-2: visit this file"
                                  'mouse-face 'highlight
                                  'keymap ted-eshell-ls-keymap)
                            ad-return-value)
       ad-return-value)))


;; OpenNextLine
;; TODO: remove it when done with smart-open-line

;; Behave like vi's o command
(defun open-next-line (arg)
  "Move to the next line and then opens a line.
    See also `newline-and-indent'."
  (interactive "p")
  (end-of-line)
  (open-line arg)
  (next-line 1)
  (when newline-and-indent
    (indent-according-to-mode)))

;; Behave like vi's O command
(defun open-previous-line (arg)
  "Open a new line before the current one.
     See also `newline-and-indent'."
  (interactive "p")
  (beginning-of-line)
  (open-line arg)
  (when newline-and-indent
    (indent-according-to-mode)))

;; Autoindent open-*-lines
(defvar newline-and-indent t
  "Modify the behavior of the open-*-line functions to cause them to autoindent.")

;; Scroll PDF in other buffer
(defun wenshan-other-docview-buffer-scroll-down ()
  "There are two visible buffers, one for taking notes and one
for displaying PDF, and the focus is on the notes buffer. This
command moves the PDF buffer forward."
  (interactive)
  (other-window 1)
  (doc-view-scroll-up-or-next-page)
  (other-window 1))

(defun wenshan-other-docview-buffer-scroll-up ()
  "There are two visible buffers, one for taking notes and one
for displaying PDF, and the focus is on the notes buffer. This
command moves the PDF buffer backward."
  (interactive)
  (other-window 1)
  (doc-view-scroll-down-or-previous-page)
  (other-window 1))

;; exchange region
(defvar exchange-pending-overlay nil)
(defvar exchange-pending-face 'cursor)

(defadvice keyboard-quit (after exchange-complete activate)
  (delete-overlay exchange-pending-overlay)
  (setq exchange-pending-overlay nil))

(defadvice cua-paste (around exchange-start activate)
  (if (and (called-interactively-p 'interactive)
           (eq last-command 'kill-region))
      (progn
        (setq exchange-pending-overlay
              (make-overlay (point) (1+ (point))))
        (overlay-put exchange-pending-overlay
                     'face exchange-pending-face))
    (progn
      (when exchange-pending-overlay
        (delete-overlay exchange-pending-overlay)
        (setq exchange-pending-overlay nil))
      ad-do-it)))

(defadvice kill-region (around exchange-exec activate)
  (if (not (and (called-interactively-p 'interactive) transient-mark-mode mark-active
                exchange-pending-overlay))
      ad-do-it
    (let* ((str (buffer-substring (region-beginning) (region-end)))
           (pending-pos (overlay-start exchange-pending-overlay))
           (pos (+ (region-beginning)
                   (if (< pending-pos (point)) (length str) 0))))
      (delete-region (region-beginning) (region-end))
      (goto-char (overlay-start exchange-pending-overlay))
      (delete-overlay exchange-pending-overlay)
      (setq exchange-pending-overlay nil)
      (insert str)
      (goto-char pos)
      (setq this-command 'kill-region))))
