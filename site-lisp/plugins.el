;;; plugins.el --- This file contain plugins packages management. See getelget.el also -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;; Copyright (C) 2012  Yuriy V. Pitomets <pitometsu@gmail.com>
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(eval-when-compile
  (require 'cl))

;; user package directory
(setq el-get-user-package-directory "~/.emacs.d/inits/")

;; additional packages sources
(setq el-get-sources
      '((:name workspaces
               :depends (escreen)
               :type http
               :url "http://grapevine.net.au/~striggs/elisp/workspaces.el"
               :description "Really just a wrapper around Noah Friedman's escreen."
               :features (workspaces))
        (:name zjl-hl
               :depends (highlight region-list-edit)
               :type http
               :url "http://www.emacswiki.org/emacs/download/zjl-hl.el"
               :website "http://www.emacswiki.org/emacs/zjl-hl.el"
               :description "highlight variable and function call and others in c/emacs"
               :features (zjl-hl))
        (:name hideshow-org
               :type git
               :url "git://github.com/secelis/hideshow-org.git"
               :website "https://github.com/secelis/hideshow-org"
               :description "provides an org-mode like interface to the hideshow.el file"
               :features (hideshow-org))
        (:name auto-complete
               :depends (popup)
               :type git
               :url "git://github.com/auto-complete/auto-complete.git"
               :website "https://github.com/auto-complete/auto-complete"
               :description "Auto Complete Mode"
               :features (auto-complete auto-complete-config))
        (:name scel
               :load-path ("el")
               :type git
               :url "git@bitbucket.org:Kamiel/scel-3.5.0.git"
               :website "http://supercollider.sourceforge.net/"
               :description "SuperCollider/Emacs interface"
               :features (scel))
        (:name emacs-xcode
               :type git
               :url "https://github.com/senny/emacs-xcode.git"
               :website "https://github.com/senny/emacs-xcode"
               :description "Emacs-xcode allows you to use xcode specific features inside your favourite text editor Emacs."
               :load "emacs-xcode.elc"
               :features (xcode))
        (:name ezbl
               :type git
               :url "https://github.com/haxney/ezbl.git"
               :website "https://github.com/haxney/ezbl"
               :description "Ezbl - Emacs interface for Uzbl (uzbl.org) This package creates an Emacs interface for Uzbl, allowing you to control Uzbl from within Emacs."
               :features (ezbl))
        (:name mac-classic-theme
               :type git
               :url "https://github.com/ahobson/mac-classic-theme.git"
               :website "https://github.com/ahobson/mac-classic-theme"
               :description "A emacs colour theme which resembles the TextMate Mac Classic colour theme."
               :features (mac-classic-theme))
        (:name ido-hacks-24
               :type git
               :url "https://github.com/scottjad/ido-hacks.git"
               :website "https://github.com/scottjad/ido-hacks"
               :description "Misc collection of ido changes, including making it behave better with dired’s copying and renaming commands (such as putting directory as first option)."
               :features (ido-hacks-24))
        (:name company
               :type elpa
               :website "http://nschum.de/src/emacs/company-mode/"
               :description "Flexible inline text and code completion"
               :features (company))
        (:name ac-company
               :depends (company auto-complete)
               :type http
               :url "https://raw.github.com/buzztaiki/auto-complete/master/ac-company.el"
               :website "http://github.com/buzztaiki/auto-complete"
               :description "company backend for auto-complete"
               :features (ac-company))
        (:name yasnippet
               :type git
               :url "git://github.com/capitaomorte/yasnippet.git"
               :website "https://github.com/capitaomorte/yasnippet"
               :description "YASnippet is a template system for Emacs."
               :features (yasnippet))
        (:name helm
               :type github
               :description "Emacs incremental and narrowing framework"
               :pkgname "emacs-helm/helm"
               :features (helm-config)
               :compile nil)
        (:name xcode-document-viewer
               :depends (emacs-w3m helm)
               :type github
               :url "https://github.com/ypitomets/emacs-xcode-document-viewer.git"
               :website "https://github.com/ypitomets/emacs-xcode-document-viewer"
               :description "xcode document viewer"
               :features (xcode-document-viewer))
        (:name dedicated
               :type elpa
               :description "A very simple minor mode for dedicated buffers"
               :features (dedicated))
        (:name ido-yes-or-no
               :type elpa
               :description "Use Ido to answer yes-or-no questions"
               :features (ido-yes-or-no))
        (:name ido-ubiquitous
               :type elpa
               :description "Use ido (nearly) everywhere."
               :features (ido-ubiquitous))
        (:name ido-better-flex
               :type elpa
               :description "A better flex (fuzzy) algorithm for Ido"
               :features (ido-better-flex))
        (:name alert
               :type git
               :url "https://github.com/jwiegley/alert.git"
               :website "https://github.com/jwiegley/alert"
               :description "Alert is a Growl-workalike for Emacs which uses a common notification interface and multiple, selectable \"styles\", whose use is fully customizable by the user."
               :features (alert))
        (:name yank-pop-summary
               :type http
               :url "https://raw.github.com/critical-bug/dot.emacs.d/master/yank-pop-summary.el"
               :website "http://www.namazu.org/~tsuchiya/elisp/index.html"
               :description "yank pop forward"
               :features (yank-pop-summary))
        (:name thing-cmds
               :depends (hide-comnt thingatpt+)
               :type git
               :url "git://github.com/emacsmirror/thing-cmds.git"
               :website "https://github.com/emacsmirror/thing-cmds"
               :description "Commands that use things, as defined by `thingatpt.el'."
               :features (thing-cmds))
        (:name ac-predictive
               :depends (auto-complete predictive)
               :type git
               :url "git://github.com/tkosaka/ac-predictive.git"
               :website "https://github.com/tkosaka/ac-predictive"
               :description "Auto-complete source for predictive-mode"
               :features (ac-predictive))
        (:name hungry-delete
               :type elpa
               :website "https://github.com/nflath/hungry-delete"
               :description "hungry delete minor mode"
               :features (hungry-delete))
        (:name globrep
               :depends (query)
               :type http
               :url "https://raw.github.com/emacsmirror/emacswiki.org/master/globrep.el"
               :website "http://www.emacswiki.org/emacs/GlobRep"
               :description "This program can be used to search for and replace strings in multiple files"
               :features (globrep))
        (:name ack
               :type git
               :url "https://github.com/leoliu/ack-el.git"
               :website "https://github.com/leoliu/ack-el"
               :description "Emacs Interface to command-line tool ack"
               :features (ack))
        (:name ack-and-a-half
               :type elpa
               :website "https://github.com/jhelwig/ack-and-a-half"
               :description "Yet another front-end for ack"
               :features (ack-and-a-half))
        (:name icicles
               :type git
               :url "git://github.com/emacsmirror/icicles.git"
               :website "https://github.com/emacsmirror/icicles"
               :description "Minibuffer input completion and cycling."
               :features (icicles))
        (:name elscreen
               :type github
               :url "https://github.com/shosti/elscreen.git"
               :website "https://github.com/shosti/elscreen"
               :description "Screen Manager for Emacsen"
               :features (elscreen))
        (:name auto-complete-clang-async
               :depends (auto-complete)
               :build ("make")
               :type github
               :url "https://github.com/Golevka/emacs-clang-complete-async.git"
               ;; :branch "test"
               :website "https://github.com/Golevka/emacs-clang-complete-async"
               :description "Complete C/C++ code, it uses libclang to parse the source code on the fly and provides completion candidates to auto-complete"
               :features (auto-complete-clang-async))
        (:name kdcomplete
               :depends (auto-complete)
               :type git
               :url "https://github.com/Pitometsu/kdcomplete.git"
               :website "https://github.com/BinaryPeak/kdcomplete"
               :description "A very fast asynchronous code completion engine for emacs. Uses auto-complete with support for yasnippet, and integrates well with clang for C/C++/Objective C completion"
               :features (kdcomplete))
        (:name mk-project
               :depends (xcscope)
               :type github
               :url "https://github.com/rakete/mk-project.git"
               :website "http://github.com/rakete/mk-project"
               :description "A Emacs project library. Quickly switch between projects and perform operations on a per-project basis."
               :features (mk-project))
        (:name project-mode
               :depends (levenshtein)
               :type svn
               :url "http://emacs-project-mode.googlecode.com/svn/trunk/"
               :website "http://code.google.com/p/emacs-project-mode/"
               :description "Emacs global minor mode for defining and navigating projects"
               :features (project-mode))
        (:name eproject
               :type git
               :url "http://repo.or.cz/r/eproject.git"
               :website "http://www.emacswiki.org/emacs/eproject"
               :description "project workspaces for emacs"
               :features (eproject))
        (:name load-directory
               :type http
               :url "http://www.cb1.com/~john/computing/emacs/lisp/basics/load-directory.el"
               :description "load all the .el or .elc files in a directory"
               :features (load-directory))
        (:name nlinum
               :type github
               :url "https://github.com/emacsmirror/nlinum.git"
               :website "https://github.com/emacsmirror/nlinum"
               :description "This is like linum-mode, but uses jit-lock to be (hopefully) more efficient."
               :features (nlinum))
        (:name git-gutter
               :type github
               :url "https://github.com/syohex/emacs-git-gutter.git"
               :website "https://github.com/syohex/emacs-git-gutter"
               :description "port of GitGutter which is a plugin of Sublime Text."
               :features (git-gutter))
        (:name diff-hl
               :type github
               :url "https://github.com/dgutov/diff-hl.git"
               :website "https://github.com/dgutov/diff-hl"
               :description "highlights uncommitted changes on the left fringe of the window, allows you to jump between and revert them selectively."
               :features (diff-hl))
        (:name svg-mode-line-themes
               :depends (xmlgen)
               :type github
               :url "https://github.com/sabof/svg-mode-line-themes.git"
               :website "https://github.com/sabof/svg-mode-line-themes"
               :description "Awesome mode-line for emacs!"
               :features (svg-mode-line-themes))
        (:name xmlgen
               :type github
               :url "https://github.com/philjackson/xmlgen.git"
               :website "https://github.com/philjackson/xmlgen"
               :description "Generate xml using sexps with the function `xmlgen'")
        (:name ob-objc
               :type github
               :url "https://gist.github.com/1492592.git"
               :website "https://gist.github.com/tharpa/1492592"
               :description "org-babel functions for Objective-C languages")
        (:name docsetutil
               :type github
               :url "https://github.com/leoliu/docsetutil-el.git"
               :website "https://github.com/leoliu/docsetutil-el"
               :description "Emacs Interface to docsetutil on OSX"
               :features (docsetutil))
        (:name org-jira
               :depends (soap-client)
               :type github
               :url "https://github.com/baohaojun/org-jira.git"
               :website "https://github.com/baohaojun/org-jira"
               :description "Use Jira in Emacs org-mode."
               :features (org-jira)
               :compile nil)
        (:name soap-client
               :type hg
               :url "https://code.google.com/p/emacs-soap-client/"
               :website "http://code.google.com/p/emacs-soap-client/"
               :description "The soap-client.el library provides access to SOAP web-services from Emacs. It supports encoding/decoding SOAP messages based on WSDL descriptors."
               :features (soap-client))
        (:name leuven-theme
               :type github
               :url "https://github.com/fniessen/emacs-leuven-theme.git"
               :website "https://github.com/fniessen/emacs-leuven-theme"
               :description "Elegant color theme for light backgrounds, with built-in style for many components such as Org-mode, Gnus, Dired+ and EDiff."
               :features (leuven-theme))
        (:name humane-theme
               :type hg
               :url "ssh://hg@bitbucket.org/jfm/color-theme-humane"
               :website "https://bitbucket.org/jfm/color-theme-humane"
               :description "Light-background color theme inspired by, but not directly based on damieng's humane theme for Visual Studio, TextMate, and XCode. It tries to prove that you can have good ergonomics without a dark background."
               :features (humane-theme))
        (:name solarized-theme
               :type github
               :url "https://github.com/bbatsov/solarized-emacs.git"
               :website "https://github.com/bbatsov/solarized-emacs"
               :description "Solarized for Emacs is an Emacs port of the Solarized theme for vim, developed by Ethan Schoonover."
               :features (solarized-theme))
        (:name helm-ls-git
               :depends (helm)
               :type github
               :url "https://github.com/emacs-helm/helm-ls-git.git"
               :website "https://github.com/emacs-helm/helm-ls-git"
               :description "Yet another helm to list git file."
               :features (helm-ls-git))
        (:name helm-descbinds
               :depends (helm)
               :type github
               :url "https://github.com/emacs-helm/helm-descbinds.git"
               :website "https://github.com/emacs-helm/helm-descbinds"
               :description "Helm Descbinds provides an interface to emacs’ `describe-bindings' making the currently active key bindings interactively searchable with helm."
               :features (helm-descbinds))
        (:name helm-c-yasnippet
               :depends (helm)
               :type github
               :url "https://github.com/emacs-helm/helm-c-yasnippet.git"
               :website "https://github.com/emacs-helm/helm-c-yasnippet"
               :description "Helm sources for yasnippet"
               :features (helm-c-yasnippet))
        (:name helm-shell-history
               :depends (helm)
               :type github
               :url "https://github.com/yuutayamada/helm-shell-history.git"
               :website "https://github.com/yuutayamada/helm-shell-history"
               :description "This program is thing that it find shell history by helm."
               :features (helm-shell-history))
        (:name helm-shell
               :depends (helm)
               :type github
               :url "https://github.com/jixiuf/helm-shell.git"
               :website "https://github.com/jixiuf/helm-shell"
               :description "Pcomplete and shell completion for helm."
               :load "helm-shell.elc")
        (:name helm-haskell-import
               :depends (helm)
               :type github
               :url "https://github.com/syohex/emacs-helm-haskell-import.git"
               :website "https://github.com/syohex/emacs-helm-haskell-import"
               :description "Port of Vim's unite-haskellimport."
               :features (helm-haskell-import))
        (:name helm-ag
               :depends (helm)
               :type github
               :url "https://github.com/syohex/emacs-helm-ag.git"
               :website "https://github.com/syohex/emacs-helm-ag"
               :description "Provides interfaces of The Silver Searcher with helm."
               :features (helm-ag))
        (:name helm-cmd-t
               :depends (helm)
               :type github
               :url "https://github.com/lewang/helm-cmd-t.git"
               :website "https://github.com/lewang/helm-cmd-t"
               :description "Provide FAST FAST FAST cmd-t completion in repositions (git, hg, etc)."
               :features (helm-cmd-t))
        (:name hsnippets
               :depends (yasnippet)
               :type github
               :url "https://github.com/polypus74/HSnippets.git"
               :website "https://github.com/polypus74/HSnippets"
               :description "Emacs YASnippet snippets for Haskell.")
        (:name restclient
               :type github
               :url "https://github.com/pashky/restclient.el.git"
               :website "https://github.com/pashky/restclient.el"
               :description "This is a tool to manually explore and test HTTP REST webservices. Runs queries from a plain-text query sheet, displays results as a pretty-printed XML, JSON and even images."
               :features (restclient))
        (:name phi-search
               :type github
               :url "https://github.com/zk-phi/phi-search.git"
               :website "https://github.com/zk-phi/phi-search"
               :description "An inferior isearch, compatible with multiple-cursors."
               :features (phi-search))
        (:name nurumacs
               :type github
               :url "https://github.com/zk-phi/nurumacs.git"
               :website "https://github.com/zk-phi/nurumacs"
               :description "Smooth-scrolling and minimap, like sublime editor"
               :features (nurumacs))
        (:name minimap
               :type github
               :url "https://github.com/razik/emacs-minimap.git"
               :website "https://github.com/razik/emacs-minimap"
               :description "Minimap sidebar for Emacs."
               :features (minimap))
        (:name skype
               :type github
               :url "https://github.com/kiwanami/emacs-skype.git"
               :website "https://github.com/kiwanami/emacs-skype"
               :description "Skype UI for emacs users."
               :features (skype))
        (:name delim-pad
               :type github
               :url "https://github.com/lewang/delim-pad.git"
               :website "https://github.com/lewang/delim-pad"
               :description "This package helps to manage space padding around paired delimiters."
               :features (delim-pad))
        (:name xml-poly
               :type svn
               :url "http://svn.meadowy.org/emacs-xml/trunk"
               :website "http://www.meadowy.org/xml/"
               :description "XML-poly is an XML parser on Emacs. Unlike the former xml.el, XML-poly treats XML names as symbols, of course it can distinguish them by their XML namespace URI."
               :pkgname "XML-poly"
               :features (xml-poly))
        (:name stripe-buffer
               :type github
               :url "https://github.com/sabof/stripe-buffer.git"
               :website "https://github.com/sabof/stripe-buffer"
               :description "Use different background colors for even and odd lines."
               :features (stripe-buffer))
        (:name vh-scroll
               :type http
               :url "http://www.splode.com/~friedman/software/emacs-lisp/src/vh-scroll.el"
               :website "https://github.com/emacsattic/vh-scroll"
               :description "vert. and horiz. scrolling that preserves point visible."
               :features (vh-scroll))
        (:name emud
               :type github
               :url "https://github.com/juster/emud.git"
               :website "https://github.com/juster/emud"
               :description "Emacs MUD Client."
               :features (emud))
        (:name keyfreq
               :type github
               :url "https://github.com/dacap/keyfreq.git"
               :website "https://github.com/dacap/keyfreq"
               :description "Track Emacs commands frequency")
        (:name es-lib
               :type github
               :url "https://github.com/sabof/es-lib.git"
               :website "https://github.com/sabof/es-lib"
               :description "A collecton of emacs utilities")
        (:name shift-text
               :depends (es-lib)
               :type github
               :url "https://github.com/sabof/shift-text.git"
               :website "https://github.com/sabof/shift-text"
               :description "Move the region in 4 directions, in a way similar to Eclipse's")
        (:name litable
               :depends (dash)
               :type github
               :website "https://github.com/Fuco1/litable"
               :description "On-the-fly evaluation/substitution of emacs lisp code"
               :pkgname "Fuco1/litable")
        (:name helm-spaces
               :depends (spaces helm)
               :type github
               :website "https://github.com/yasuyk/helm-spaces"
               :description "helm sources for spaces."
               :pkgname "yasuyk/helm-spaces")
        (:name col-highlight
               :depends (vline)
               :type github
               :website "https://raw.github.com/emacsmirror/emacswiki.org/master/col-highlight.el"
               :description "Highlight the current column."
               :pkgname "emacsmirror/col-highlight")
        (:name ruby-complexity
               :depends (flymake)
               :type github
               :website "https://github.com/jsmestad/ruby-complexity"
               :description "Runs the Ruby flog tool against Ruby code as you write it. Displays the flog score at the beginning of each method in Emacs."
               :pkgname "jsmestad/ruby-complexity")
        (:name emacs-rails-reloaded
               :type github
               :website "https://github.com/tychobrailleur/emacs-rails-reloaded"
               :description "It is the minor mode for editing Ruby On Rails code with Emacs. This minor mode makes your work much easier and user friendly."
               :pkgname "tychobrailleur/emacs-rails-reloaded"
               :features (rails-autoload))
        (:name Enhanced-Ruby-Mode
               :description "Replacement for ruby-mode which uses ruby 1.9's Ripper to parse and indent"
               :type github
               :pkgname "jacott/Enhanced-Ruby-Mode"
               :features ruby-mode)
        (:name hrb-mode
               :description "hrb-mode is an Emacs minor-mode for highlighting Ruby blocks just like (show-paren-mode t)."
               :type github
               :pkgname "ckruse/hrb-mode")
        (:name ruby-tools
               :description "Ruby tools is a collection of handy functions for Emacs ruby-mode. You can turn a string to symbol, symbol to string, single to double quote string, double to single quote string, clear string, interpolate and more..."
               :type github
               :pkgname "rejeep/ruby-tools")
        (:name rspec-mode
               :description "RSpec mode provides some convenience functions for dealing with RSpec."
               :type github
               :pkgname "dgtized/rspec-mode")
        (:name auto-complete-ruby
               :description "Auto-complete sources for Ruby"
               :type http
               :url "http://www.cx4a.org/pub/auto-complete-ruby.el"
               :depends (auto-complete rcodetools))
        (:name emacs-anzu
               :description "anzu.el is Emacs port of anzu.vim. anzu.el provides minor mode which display current point and total matched in various search mode."
               :type github
               :pkgname "syohex/emacs-anzu")
        (:name google-translate
               :description "Invoking the function google-translate-query-translate queries the source and target languages and text to translate, and shows a buffer with available translations of the text."
               :type github
               :pkgname "manzyuk/google-translate")
        (:name smart-mode-line
               :description "A fixed width smart mode line for Emacs."
               :type github
               :pkgname "Bruce-Connor/smart-mode-line")
        (:name highlight-80+
               :description "This mode highlights all characters that cross the 80 character line limit."
               :type http
               :url "http://nschum.de/src/emacs/highlight-80+/highlight-80+.el")
        (:name emacs-pry
               :description "support for running Pry within in Emacs"
               :type github
               :pkgname "jacott/emacs-pry"
               :features pry)
        (:name yard-mode
               :description "Rudimentary support for fontifying YARD tags and directives in ruby comments."
               :type github
               :pkgname "/pd/yard-mode.el")
        (:name helm-swoop
               :description "List the multi lines to another buffer, which is able to squeeze by any words you input. At the same time, the original buffer's cursor is jumping line to line according to moving up and down the line list."
               :type github
               :pkgname "ShingoFukuyama/helm-swoop")
        (:name helm-git-grep
               :description "helm for [git grep], an incremental git-grep"
               :type github
               :pkgname "yasuyk/helm-git-grep")
        (:name free-keys
               :description "Show free bindings in current buffer."
               :type github
               :pkgname "Fuco1/free-keys")
        (:name xterm-extras
               :description "define additional function key sequences for recent versions of xterm"
               :type github
               :pkgname "yyr/xterm-extras.el")
        (:name workgroups2
               :description "Workgroups is a session manager for Emacs."
               :load-path ("src")
               :type github
               :pkgname "pashinin/workgroups2"
               :features "workgroups2")
        (:name ruby-hash-syntax
               :description "Adapted from the method used by TextMate, this library provides a command ruby-hash-syntax-toggle which attempts to automatically convert the selected region of ruby code between 1.8 and 1.9 hash styles."
               :type github
               :pkgname "purcell/ruby-hash-syntax")
        (:name ruby-refactor
               :description "Ruby refactor is inspired by the Vim plugin vim-refactoring-ruby"
               :type github
               :pkgname "ajvargo/ruby-refactor")
        (:name ac-rsense-expand
               :description "EmacsでRsenseを利用する際、補完したメソッドを yasnippetで展開するためのEmacs設定及びsnippet群です。"
               :type github
               :pkgname "uskanda/ac-rsense-yas-expand")
        (:name rubocop
               :description "A simple Emacs interface for RuboCop."
               :type github
               :pkgname "bbatsov/rubocop-emacs")
        (:name inflections
               :description "convert english words between singular and plural"
               :website "convert english words between singular and plural"
               :type http
               :url "https://raw.githubusercontent.com/eschulte/jump.el/master/inflections.el")
        (:name projectile-rails
               :description "Projectile Rails is a minor mode for working with the Rails project in GNU Emacs. Internally it based on Projectile."
               :type github
               :pkgname "asok/projectile-rails")
        (:name dbgr
               :website "https://github.com/rocky/emacs-dbgr"
               :description "The Grand \"Cathedral\" Debugger rewrite: Towards a modular framework for interacting with external debuggers."
               :depends (test-simple load-relative loc-changes)
               :type github
               :pkgname "rocky/emacs-dbgr"
               :build (let ((load-path-env (mapconcat (quote identity) load-path ":")))
                        (mapcar (lambda (command)
                                  (list "sh" "-c" (format "EMACSLOADPATH=%s %s" (shell-quote-argument load-path-env) (shell-quote-argument command))))
                                (quote ("./autogen.sh" "./configure" "make"))))
               :features (loc-changes load-relative test-simple realgud))
        (:name helm-robe
               :description "provides function for setting robe-completing-read-func"
               :type github
               :pkgname "syohex/emacs-helm-robe")
        (:name devlock
               :website "http://www.emacswiki.org/emacs/DevelockMode"
               :description "Nice little minor-mode that will make your buffer all nice and flashy when you break some formatting rules"
               :type http
               :url "http://www.jpl.org/ftp/pub/elisp/develock.el.gz"
               :build ("gunzip -c develock.el.gz > develock.el")
               :load    "develock.el"
               :compile "develock.el"
               :features (develock))
        (:name doxymacs-yard
               :description "Based on Jonas's doxymacs-yard.el, just added font lock for yard tags."
               :depends (doxymacs)
               :type github
               :pkgname "doitian/doxymacs-yard")
        (:name ergoemacs-mode
               :description "ErgoEmacs keybindings improves GNU Emacs for people who did not grew up with Emacs"
               :website "http://ergoemacs.org"
               :type github
               :pkgname "ergoemacs/ergoemacs-mode"
               :features (ergoemacs-mode))
        (:name makey
               :description "interactive commandline mode"
               :type github
               :pkgname "mickeynp/makey")
        (:name discover
               :depends (makey)
               :description "discover more of Emacs"
               :website "http://www.masteringemacs.org/articles/2013/12/21/discoverel-discover-emacs-context-menus"
               :type github
               :pkgname "mickeynp/discover.el")
        (:name guide-key
               :depends (popwin)
               :description "displays the available key bindings automatically and dynamically."
               :type github
               :pkgname "kbkbkbkb1/guide-key")
        (:name myrth-theme
               :description "Eye-candy light clear minimalistic color theme."
               :website "https://bitbucket.org/Kamiel/myrth"
               :type git
               :url "https://Kamiel@bitbucket.org/Kamiel/myrth.git"
               :prepare (add-to-list (quote custom-theme-load-path) default-directory))
        (:name base16-emacs
               :description "Base 16 themes set ported to Emacs"
               :website "https://github.com/neil477/base16-emacs"
               :type github
               :pkgname "neil477/base16-emacs"
               :prepare (add-to-list (quote custom-theme-load-path) default-directory))
        (:name objc-font-lock
               :description "Highlight Objective-C method calls"
               :website "https://github.com/Lindydancer/objc-font-lock"
               :type github
               :pkgname "Lindydancer/objc-font-lock")
        (:name auto-complete-clang-objc
               :description "Emacs auto-complete clang backend for Objective C devlepment for iOS"
               :website "https://github.com/yasuyk/auto-complete-clang-objc"
               :type github
               :pkgname "yasuyk/auto-complete-clang-objc")
        (:name manage-minor-mode
               :description "Manage your minor-mode on the dedicated interface buffer."
               :website "https://github.com/ShingoFukuyama/manage-minor-mode"
               :type github
               :pkgname "ShingoFukuyama/manage-minor-mode")
        (:name ecb
               :description "Emacs Code Browser"
               :type github
               :pkgname "alexott/ecb")
        (:name git-gutter-fringe+
               :description "git-gutter-fringe+ is a display mode for git-gutter+.el. It uses the buffer fringe instead of the buffer margin."
               :depends (git-gutter+ fringe-helper)
               :type github
               :pkgname "nonsequitur/git-gutter-fringe-plus")
        (:name git-gutter+
               :description "View, stage and revert Git changes straight from the buffer."
               :type github
               :pkgname "nonsequitur/git-gutter-plus")
        (:name windsize
               :description "Simple, intuitive window resizing"
               :type github
               :pkgname "grammati/windsize")
        (:name sunrise-commander
               :description "Twin-pane file manager for Emacs based on Dired and inspired by MC."
               :type github
               :pkgname "escherdragon/sunrise-commander")
        (:name adaptive-wrap
               :description "Smart line-wrapping with wrap-prefix"
               :type github
               :pkgname "emacsmirror/adaptive-wrap")
        (:name rainbow-mode
               :description "Colorize color names in buffers"
               :website "http://elpa.gnu.org/packages/rainbow-mode.html"
               :type http
               :url "http://elpa.gnu.org/packages/rainbow-mode-0.9.el")
        (:name rdoc-mode
               :description "Major mode for RDoc editing"
               :website "https://github.com/jwiegley/ruby-mode"
               :type http
               :url "https://raw.githubusercontent.com/jwiegley/ruby-mode/master/rdoc-mode.el")
        (:name howdoi
               :description "Emacs interface to the `howdoi` command line tool (https://github.com/gleitz/howdoi)"
               :type github
               :pkgname "atykhonov/emacs-howdoi")
        (:name helm-google
               :description "Emacs Helm Interface for quick Google searches"
               :type github
               :pkgname "steckerhalter/helm-google")
        (:name seqential-command
               :description "Many commands into one command"
               :type github
               :pkgname "HKey/sequential-command")
        (:name nrepl-eval-sexp-fu
               :description "eval-sexp-fu.el modified for use with nrepl"
               :depends (highlight smartparens)
               :type github
               :pkgname "samaaron/nrepl-eval-sexp-fu")
        (:name electric-case
               :description "insert camelCase and snake_case words, without “Shift”ing"
               :type github
               :pkgname "zk-phi/electric-case")
        (:name cedit
               :description "paredit-like commands for c-like languages"
               :type github
               :pkgname "zk-phi/cedit")
        (:name clean-aindent
               :description "Emacs extension for clean auto-indent and backspace unindent"
               :type github
               :pkgname "pmarinov/clean-aindent")
        (:name swift-mode
               :description "Major-mode for Apple's Swift programming language"
               :type github
               :pkgname "chrisbarrett/swift-mode")
        (:name req-package
               :description "req-package is a macro wrapper on top of use-package. It's goal is to simplify package dependencies management, when using use-package for your .emacs."
               :type github
               :pkgname "edvorg/req-package")
        (:name dictem
               :website "http://dictem.sourceforge.net/"
               :description "dict lookup"
               :type http-tar
               :options ("xzf")
               :url "http://www.mova.org/~cheusov/pub/dictem/dictem-1.0.4.tar.gz")))

;; packages list
(setq el-get-packages
      '(el-get

        ;; ~ ENVIRONMENT ~
        ;; notify
        alert
        edit-server
        f
        s
        dash
        ; at-el ; naming not so cool as in namespace
        ; namespace ; <- standard packages use

        ;; ~ ACCESSORY ~
        powerline
        diminish
        stripe-buffer
        rebox2
        pastie
        dictem
        google-translate
        thesaurus
        quickrun
        emacs-sos
        myrth-theme

        ;; ~ UTILITIES ~
        edbi
        multi-term
        feature-mode
        regex-tool
        typing
        shell-switcher
        howdoi
        helm-google

        ;; ~ GIT ~
        magit

        ;; ~ ARDUINO ~
        arduino-mode

        ;; ~ WORKFLOW ~
        smartparens
        emacs-anzu
        auto-indent-mode
        clean-aindent
        col-highlight
        highline
        lacarte
        pp-c-l
        textmate
        undo-tree
        whole-line-or-region
        yasnippet
        helm
        auto-complete
        goto-last-change
        phi-search
        highlight-indentation
        helm-c-yasnippet
        helm-descbinds
        helm-ls-git
        helm-cmd-t
        helm-swoop
        helm-git-grep
        smooth-scrolling
        jump-char
        projectile
        fastnav
        flymake-cursor
        zencoding-mode
        multiple-cursors
        highlight-symbol
        expand-region
        drag-stuff
        volatile-highlights
        ace-jump-mode
        thing-cmds
        quick-jump
        yank-pop-summary
        delim-pad
        git-gutter-fringe+
        ;; ergoemacs-mode
        guide-key
        manage-minor-mode
        visual-regexp
        seqential-command
        nrepl-eval-sexp-fu
        ;; fixmee
        electric-case
        srep

        ;; ~ WINDOWS ~
        window-number
        windsize
        buffer-move
        sunrise-commander
        dired+
        popwin
        sr-speedbar
        workgroups2
        direx

        ;; ~ ERGONOMIC ~
        keyfreq
        free-keys

        ;; ~ OBJC ~
        objc-font-lock
        flydoc
        auto-complete-clang-objc
        adaptive-wrap
        cedit
        swift-mode

        ;; ~ RUBY ~
        auto-complete-ruby
        inf-ruby
        rdoc-mode
        robe-mode
        helm-robe
        rvm
        rubocop
        emacs-pry
        ruby-complexity
        rcodetools
        enh-ruby-mode
        ruby-tools
        rspec-mode
        ruby-end
        ruby-hash-syntax
        ruby-refactor
        ac-rsense-expand
        flymake-ruby
        yari
        bundler
        ;; dbgr
        yard-mode

        ;; ~ RAILS ~
        helm-rails
        projectile-rails

        ;; ~ WEB ~
        restclient
        json-mode
        markdown-mode
        coffee-mode
        js2-mode
        yaml-mode
        haml-mode
        slim-mode
        sass-mode
        rainbow-mode
        htmlize
        web-mode
        emacs-w3m))

;; bootstrap el-get if necessary and load the specified packages
(load "getelget")

;; install missing packages
(let ((emacs-lisp-mode-hook nil)
      (el-get-git-shallow-clone t)
      (el-get-is-lazy t)
      (el-get-user-package-directory (concat (file-name-as-directory user-emacs-directory)
                                             "inits")))
  (el-get-sync))

;; remove unnecessary packages
(defun el-get-clean ()
  "Cleanup unnecessary packages. See `el-get-cleanup'."
  (interactive)
  (el-get-cleanup el-get-packages))
(el-get-clean)

;; helper macro for multiply plugin interaction
(defmacro after-mode (mode &rest body)
  "`eval-after-load' MODE evaluate BODY."
  (declare (indent defun))
  `(if (featurep ,mode)
       (progn ,@body)
     (eval-after-load ,mode
       '(progn ,@body))))

(lexical-let* ((keywoard "(\\<\\(after-mode\\)\\>")
               (feature (concat keywoard " +'\\([^() ]+\\)")))
  (font-lock-add-keywords 'emacs-lisp-mode
                          `((,keywoard (1 'font-lock-keyword-face))
                            (,feature (2 'font-lock-constant-face)))))
