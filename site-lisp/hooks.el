;;; hooks.el --- This file contain hooks for different modes -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;; Copyright (C) 2012  Yuriy V. Pitomets <pitometsu@gmail.com>
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; search at both stackoverflow and google code
(add-hook 'prog-mode-hook
          '(lambda () (local-set-key (kbd "C-c ; s") 'w3mext-hacker-search)))

;; Ruby mode
(setq-default ruby-mode-hook nil)
(add-hook 'ruby-mode-hook 'yard-mode)
(add-hook 'ruby-mode-hook 'turn-on-auto-fill)
(add-hook 'ruby-mode-hook 'auto-indent-mode-on)
(add-hook 'ruby-mode-hook 'linum-mode)
(add-hook 'ruby-mode-hook '(lambda () (hl-line-mode 1)))
(add-hook 'ruby-mode-hook '(lambda () (highlight-indentation-current-column-mode 1)))
(add-hook 'ruby-mode-hook '(lambda () (yas-minor-mode-on)))
(add-hook 'ruby-mode-hook '(lambda ()
							 (when (and buffer-file-name (string-match "_spec.rb$" buffer-file-name))
							   (rspec-mode))))
(add-hook 'ruby-mode-hook 'ruby-tools-mode)
(add-hook 'ruby-mode-hook 'imenu-add-menubar-index)
(add-hook 'ruby-mode-hook 'robe-mode)
(add-hook 'ruby-mode-hook 'eldoc-mode)
(add-hook 'ruby-mode-hook '(lambda () (rvm-activate-corresponding-ruby)))
(add-hook 'ruby-mode-hook '(lambda () (setq ac-sources
											(append '(ac-ruby-sources
													  ac-source-robe)
													ac-sources))
							 (ac-ruby-setup)
							 (auto-complete-mode 1)))
(add-hook 'ruby-mode-hook 'ruby-refactor-mode-launch)
(add-hook 'ruby-mode-hook '(lambda ()
							 (add-to-list 'write-file-functions
										  'delete-trailing-whitespace)))

;; Enhanced Ruby mode
(setq-default enh-ruby-mode-hook (append enh-ruby-mode-hook ruby-mode-hook))

;; CSS
(add-hook 'css-mode-hook 'rainbow-mode)

;; Web mode
(add-hook 'web-mode-hook '(lambda () (hs-minor-mode 1)))
(add-hook 'web-mode-hook '(lambda () (hs-org/minor-mode 1)))
(add-hook 'web-mode-hook 'highline-mode-on)
(add-hook 'web-mode-hook 'linum-mode)
(add-hook 'web-mode-hook 'auto-indent-mode-on)
(add-hook 'web-mode-hook '(lambda () (zencoding-mode t)))
(add-hook 'web-mode-hook '(lambda () (yas-minor-mode-on)))

;; nXML mode hook
(add-hook 'nxml-mode-hook '(lambda () (hs-minor-mode 1)))
(add-hook 'nxml-mode-hook '(lambda () (hs-org/minor-mode 1)))
(add-hook 'nxml-mode-hook 'highline-mode-on)
(add-hook 'nxml-mode-hook 'linum-mode)
(add-hook 'web-mode-hook 'auto-indent-mode-on)
(add-hook 'nxml-mode-hook '(lambda () (zencoding-mode t)))
(add-hook 'nxml-mode-hook '(lambda () (yas-minor-mode-on)))
