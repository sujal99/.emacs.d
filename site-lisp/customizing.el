;;; customizing.el --- This file contain some plugins compatibility fixes -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;; Copyright (C) 2012  Yuriy V. Pitomets <pitometsu@gmail.com>
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; enable whole line or region mode
(after-mode 'whole-line-or-region-mode
  (whole-line-or-region-mode 1))

;; fix jump-char keymap
(after-mode 'jump-char
  (define-key jump-char-isearch-map (kbd jump-char-forward-key) 'jump-char-repeat-forward)
  (define-key jump-char-isearch-map (kbd jump-char-backward-key) 'jump-char-repeat-backward))
