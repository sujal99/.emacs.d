;;; init.el --- The EMACS initialization file -*- lexical-binding: t; mode: emacs-lisp; coding: utf-8 -*-
;;
;; Copyright (C) 2012  Yuriy V. Pitomets <pitometsu@gmail.com>
;;
;; URL: https://bitbucket.org/Kamiel/.emacs.d
;;
;; This file is free software licensed under the terms of the
;; GNU General Public License, version 3 or later.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; Faces

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:height 120 :family "Inconsolata LGC")))))


;; Pre-configuring

;; js-mode hook
(load "js")

;; ruby-mode map
(load "ruby-mode")

;; org-mode map
(load "org")

;; path to local config
(defvar user-emacs-directory "~/.emacs.d/" "Path to init EMACS directory.")
(add-to-list 'load-path
             (concat
              (file-name-as-directory user-emacs-directory) "site-lisp/"))

;; plugins
(load "plugins")

;; hacks
(load "hacks") ; defun imenu-elisp-sections


;; Variables

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-auto-show-menu 0.07)
 '(ac-auto-start 1)
 '(ac-delay 0.01)
 '(ac-dictionary-directories (quote ("/Users/user/.emacs.d/el-get/auto-complete/dict" "/Users/user/.emacs.d/ac-dict")))
 '(ac-menu-height 12)
 '(ac-modes (quote (emacs-lisp-mode lisp-mode lisp-interaction-mode slime-repl-mode c-mode cc-mode c++-mode java-mode malabar-mode clojure-mode scala-mode scheme-mode sclang-mode ocaml-mode tuareg-mode haskell-mode perl-mode cperl-mode python-mode ruby-mode ecmascript-mode javascript-mode js-mode js2-mode php-mode css-mode makefile-mode sh-mode fortran-mode f90-mode ada-mode xml-mode sgml-mode ts-mode objc-mode)))
 '(ac-quick-help-delay 0.01)
 '(ac-quick-help-prefer-pos-tip nil)
 '(ack-and-a-half-arguments (quote (" --column --smart-case ")))
 '(ack-and-a-half-executable "/usr/local/bin/ack")
 '(ack-and-a-half-use-environment t)
 '(align-to-tab-stop nil)
 '(ansi-color-names-vector ["#2E3436" "#A52A2A" "ForestGreen" "darkorange2" "DodgerBlue" "#A020F0" "darkcyan" "ivory4"])
 '(anything-ack-command "/usr/local/bin/ack --nogroup --nocolor --column --smart-case")
 '(apropos-do-all t)
 '(auto-hscroll-mode t)
 '(auto-indent-assign-indent-level 2)
 '(auto-indent-backward-delete-char-behavior (quote hungry))
 '(auto-indent-blank-lines-on-move nil)
 '(auto-indent-home-is-beginning-of-indent nil)
 '(auto-indent-key-for-end-of-line-insert-char-then-newline "<C-M-return>")
 '(auto-indent-key-for-end-of-line-then-newline "<M-return>")
 '(auto-indent-kill-line-at-eol nil)
 '(auto-indent-kill-remove-extra-spaces t)
 '(auto-indent-known-text-modes (quote (text-mode message-mode fundamental-mode texinfo-mode conf-windows-mode LaTeX-mode latex-mode TeX-mode tex-mode outline-mode nroww-mode sclang-mode)))
 '(auto-indent-next-pair-timer-interval (quote ((Man-mode 1.5) (workspace-controller-mode 1.5) (js-mode 1.5) (w3m-mode 1.5) (el-get-package-menu-mode 1.5) (wl-summary-mode 1.5) (egg-status-buffer-mode 1.5) (ack-and-a-half-mode 1.5) (debugger-mode 1.5) (fundamental-mode 1.5) (apropos-mode 1.5) (org-mode 1.5) (help-mode 1.5) (sclang-mode 1.5) (markdown-mode 1.5) (Custom-mode 1.5) (objc-mode 1.5) (nxml-mode 1.5) (emacs-lisp-mode 1.5) (default 0.0005))))
 '(auto-indent-untabify-on-save-file nil)
 '(auto-indent-use-text-boundaries nil)
 '(before-save-hook nil)
 '(blank-style (quote (color)))
 '(blink-cursor-mode nil)
 '(browse-url-generic-program "firefox")
 '(c-auto-align-backslashes t)
 '(c-default-style (quote ((objc-mode . "linux") (java-mode . "java") (awk-mode . "awk") (other . "gnu"))))
 '(c-mode-hook (quote (er/add-cc-mode-expansions)))
 '(cc-other-file-alist (quote (("\\.cc\\'" (".hh" ".h")) ("\\.hh\\'" (".cc" ".C")) ("\\.c\\'" (".h")) ("\\.h\\'" (".c" ".cc" ".C" ".CC" ".cxx" ".cpp" ".m" ".mm" ".M" ".MM")) ("\\.m\\'" (".h")) ("\\.M\\'" (".H" ".h")) ("\\.mm\\'" (".h")) ("\\.MM\\'" (".H" ".h")) ("\\.C\\'" (".H" ".hh" ".h")) ("\\.H\\'" (".C" ".CC" ".M" ".MM")) ("\\.CC\\'" (".HH" ".H" ".hh" ".h")) ("\\.HH\\'" (".CC")) ("\\.c\\+\\+\\'" (".h++" ".hh" ".h")) ("\\.h\\+\\+\\'" (".c++")) ("\\.cpp\\'" (".hpp" ".hh" ".h")) ("\\.hpp\\'" (".cpp")) ("\\.cxx\\'" (".hxx" ".hh" ".h")) ("\\.hxx\\'" (".cxx")))))
 '(character-set "utf-8")
 '(coding-system "utf-8")
 '(column-number-mode t)
 '(comint-prompt-read-only t)
 '(comment-auto-fill-only-comments t)
 '(confirm-nonexistent-file-or-buffer nil)
 '(cua-enable-cua-keys nil)
 '(cua-mode t nil (cua-base))
 '(current-language-environment "UTF-8")
 '(custom-buffer-done-kill t)
 '(custom-enabled-themes (quote (myrth)))
 '(custom-safe-themes (quote ("85e06fced684eb3795e30ccc3907668c1017d6217e38c5d42ec4d37adca65b5d" default)))
 '(custom-theme-directory "~/.emacs.d/themes")
 '(default-frame-alist (quote ((width . 100) (height . 50))))
 '(default-indicate-empty-lines t t)
 '(default-input-method "utf-8")
 '(deft-directory "~/Dropbox/Org")
 '(deft-extension "org")
 '(deft-text-mode (quote org-mode))
 '(deft-use-filename-as-title t)
 '(delete-by-moving-to-trash t)
 '(delete-selection-mode t)
 '(delim-pad-mode t)
 '(develock-ruby-font-lock-keywords (quote ((develock-find-long-lines (1 (quote develock-long-line-1) t nil) (2 (quote develock-long-line-2) t nil)) (develock-find-tab-or-long-space (1 (quote develock-whitespace-2)) (2 (quote develock-whitespace-3) nil t)) ("[^
 ]\\([	 ]+\\)$" (1 (quote develock-whitespace-1) t nil)) ("\\( +\\)\\(	+\\)" (1 (quote develock-whitespace-1) t nil) (2 (quote develock-whitespace-2) t nil)) ("\\(	\\) 	" (1 (quote develock-whitespace-2) append nil)) ("^[	 ]+$" (0 (quote develock-whitespace-2) append nil)))))
 '(dired-details-hidden-string "")
 '(dired-details-hide-link-targets nil)
 '(dired-dwim-target t)
 '(dired-listing-switches "--group-directories-first -alh")
 '(dired-mode-hook (quote (diredp-nb-marked-in-mode-name (lambda nil (set make-local-variable font-lock-defaults))  (quote ((dired-font-lock-keywords dired-font-lock-keywords diredp-font-lock-keywords-1) t nil nil beginning-of-line))) (if (fboundp (quote font-lock-refresh-defaults)) dired-extra-startup (lambda nil (guide-key/add-local-guide-key-sequence "?") (guide-key/add-local-guide-key-sequence "%")) (lambda nil (set (make-local-variable (quote font-lock-defaults)) (cons (quote (dired-font-lock-keywords diredp-font-lock-keywords-1)) (cdr font-lock-defaults))) (when (fboundp (quote font-lock-refresh-defaults)) (font-lock-refresh-defaults))) (lambda nil (local-set-key (kbd "O") (quote helm-c-moccur-dired-do-moccur-by-moccur))) turn-on-stripe-buffer-mode (lambda nil (hl-line-mode 1)))))
 '(dired-omit-files "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\.")
 '(dired-recursive-copies (quote always))
 '(dired-recursive-deletes (quote always))
 '(dirtree-windata (quote (frame left 0.25 delete)))
 '(display-buffer-function (quote popwin:display-buffer))
 '(drag-stuff-global-mode t)
 '(ecb-options-version "2.40")
 '(echo-keystrokes 0.7)
 '(ediff-grab-mouse t)
 '(ediff-highlight-all-diffs t)
 '(ediff-keymap-setup-hook (quote ((lambda nil (progn (define-key ediff-mode-map (kbd "<down>") (quote ediff-next-difference)) (define-key ediff-mode-map (kbd "<up>") (quote ediff-previous-difference)))))))
 '(ediff-split-window-function (quote split-window-horizontally))
 '(ediff-window-setup-function (quote ediff-setup-windows-plain))
 '(egg-enable-tooltip t)
 '(el-get-git-shallow-clone t)
 '(el-get-growl-notify-path "/usr/local/bin/growlnotify")
 '(el-get-is-lazy t)
 '(el-get-user-package-directory "~/.emacs.d/inits/")
 '(eldoc-idle-delay 1.2)
 '(elmo-imap4-default-authenticate-type (quote clear))
 '(elmo-imap4-default-port 993)
 '(elmo-imap4-default-server "imap.gmail.com")
 '(elmo-imap4-default-stream-type (quote ssl))
 '(elmo-imap4-default-user "Pitometsu")
 '(elmo-mime-display-as-is-coding-system (quote utf8))
 '(elscreen-buffer-to-nickname-alist (quote (("[Ss]hell" . "shell") ("compilation" . "compile") ("-telnet" . "telnet") ("dict" . "OnlineDict") ("*WL:Message*" . "Wanderlust") ("*Deft*" . "Deft"))))
 '(elscreen-default-buffer-initial-major-mode (quote deft-mode))
 '(elscreen-default-buffer-name "*Deft*")
 '(elscreen-display-tab nil)
 '(emacs-lisp-mode-hook (quote (turn-on-eldoc-mode checkdoc-minor-mode turn-on-auto-fill imenu-add-menubar-index auto-indent-mode-on linum-mode highline-mode (lambda nil (auto-complete-mode 1)) (lambda nil (yas-minor-mode-on)) (lambda nil (progn (local-unset-key (kbd "C-M-x")) (define-key checkdoc-minor-mode-map (kbd "C-M-x") nil))) imenu-elisp-sections)))
 '(enable-recursive-minibuffers t)
 '(enh-ruby-extra-keywords (quote ("debugger" "private" "public" "protected" "raise" "next" "unless")))
 '(ergoemacs-keyboard-layout "programmer-dv")
 '(ergoemacs-mode-used "5.13.12-4")
 '(ergoemacs-theme "guru")
 '(escreen-goto-screen-before-hook nil)
 '(escreen-goto-screen-hook nil)
 '(escreen-new-screen-default-buffer "*Messages*")
 '(eshell-prompt-function (lambda nil (let ((user-uid-face (if (zerop (user-uid)) (quote ((t (:inherit (eshell-ls-missing eshell-prompt bold))))) (quote ((t (:inherit (eshell-ls-directory eshell-prompt bold)))))))) (concat (propertize (or (ignore-errors (format "[%s] " (vc-call-backend (vc-responsible-backend default-directory) (quote mode-line-string) default-directory))) "") (quote face) (quote ((t (:inherit (eshell-ls-clutter eshell-prompt)))))) (propertize (eshell/pwd) (quote face) (quote ((t (:inherit (eshell-prompt)))))) (propertize (format-time-string " | %Y-%m-%d %H:%M " (current-time)) (quote face) (quote ((t (:inherit (org-hide eshell-prompt)))))) (propertize "
" (quote face) (quote ((t (:inherit eshell-prompt))))) (propertize user-login-name (quote face) (quote ((t (:inherit (eshell-ls-symlink eshell-prompt)))))) (propertize "@" (quote face) user-uid-face) (propertize system-name (quote face) (quote ((t (:inherit (eshell-ls-executable eshell-prompt)))))) (if (= (user-uid) 0) (propertize " ■" (quote face) user-uid-face) (propertize " ●" (quote face) user-uid-face)) " "))))
 '(eshell-prompt-regexp "^[^■●]* [■●] ")
 '(eshell-visual-commands (quote ("vi" "screen" "top" "less" "more" "lynx" "ncftp" "pine" "tin" "trn" "elm" "htop" "mc")))
 '(etm-dont-activate t)
 '(etm-use-goto-line t)
 '(explicit-shell-file-name "/bin/bash")
 '(fancy-splash-image "~/.emacs.d/splash.png")
 '(fast-lock-cache-directories (quote ("~/.emacs.d/emacs-flc")) t)
 '(fci-always-use-textual-rule nil)
 '(fci-blank-char 8202)
 '(fci-eol-char 8201)
 '(fci-handle-truncate-lines t)
 '(fci-rule-color "#CBCBCB")
 '(fci-rule-use-dashes nil)
 '(fci-rule-width 2)
 '(fic-highlighted-words (quote ("FIXME" "FIXED" "TODO" "BUG" "OMG" "WTF" "!!!" "???")))
 '(fill-column 80)
 '(flymake-cursor-auto-enable t)
 '(flymake-gui-warnings-enabled nil)
 '(flymake-no-changes-timeout 0.5)
 '(flyspell-abbrev-p t)
 '(fringe-mode nil nil (fringe))
 '(git--timer-sec 0.4)
 '(global-anzu-mode t)
 '(global-auto-revert-mode t)
 '(global-discover-mode t)
 '(global-git-gutter+-mode t)
 '(global-git-gutter-mode t)
 '(global-smart-tab-mode t)
 '(global-widen-window-mode t)
 '(golden-ratio-exclude-buffer-names nil)
 '(golden-ratio-exclude-modes (quote ("speedbar-mode")))
 '(golden-ratio-extra-commands (quote (windmove-left windmove-right windmove-down windmove-up mouse-drag-region)))
 '(golden-ratio-inhibit-functions nil)
 '(golden-ratio-mode t)
 '(google-translate-default-source-language "en")
 '(google-translate-default-target-language "ru")
 '(google-translate-enable-ido-completion nil)
 '(google-translate-show-phonetic t)
 '(grep-command "/opt/local/bin/egrep -s --colour=auto")
 '(grep-o-matic-search-patterns (quote ("*.cpp" "*.c" "*.h" "*.awk" "*.sh" "*.py" "*.pl" "[Mm]akefile" "*.el" "*.md")))
 '(guide-key-mode t)
 '(guide-key/guide-key-sequence (quote ("M-s" "C-x" "C-c")))
 '(guide-key/idle-delay 0.3)
 '(guide-key/popup-window-position (quote bottom))
 '(guide-key/recursive-key-sequence-flag t)
 '(haskell-check-command "hlint")
 '(haskell-doc-show-global-types t)
 '(haskell-doc-use-inf-haskell t)
 '(haskell-font-lock-symbols nil)
 '(haskell-hoogle-command "hoogle")
 '(haskell-indent-offset 2)
 '(haskell-indentation-ifte-offset 2)
 '(haskell-indentation-layout-offset 2)
 '(haskell-indentation-left-offset 2)
 '(haskell-indentation-where-post-offset 2)
 '(haskell-indentation-where-pre-offset 2)
 '(haskell-mode-hook (quote (turn-on-haskell-indent turn-on-haskell-indentation turn-on-eldoc-mode turn-on-haskell-doc-mode turn-on-haskell-decl-scan imenu-add-menubar-index (lambda nil "This get called after haskell mode is enabled." (setq outline-regexp "^[^	 ].*\\|^.*[	 ]+\\(where\\|of\\|do\\|in\\|if\\|then\\|else\\|let\\|module\\|import\\|deriving\\|instance\\|class\\)[
 ]") (setq outline-level (quote hsk-outline-level)) (outline-minor-mode t)) (lambda nil (flymake-mode 1) (ghc-init)) (lambda nil (set-fill-column 70)) turn-on-auto-fill linum-mode auto-indent-mode-on (lambda nil (flash-column-highlight)) turn-on-fci-mode (lambda nil (highlight-symbol-mode 1)) highline-mode (lambda nil (auto-complete-mode 1)) (lambda nil (yas-minor-mode-on)) (lambda nil (local-set-key (kbd "C-c l") (quote hs-lint))))))
 '(haskell-process-type (quote cabal-dev))
 '(haskell-stylish-on-save t)
 '(haskell-tags-on-save nil)
 '(helm-adaptive-history-length 128)
 '(helm-adaptive-mode t nil (helm-adaptive))
 '(helm-ag-command-option "--all-text")
 '(helm-ag-insert-at-point (quote symbol))
 '(helm-always-two-windows nil)
 '(helm-buffer-max-length nil)
 '(helm-buffers-favorite-modes (quote (lisp-interaction-mode emacs-lisp-mode text-mode org-mode objc-mode)))
 '(helm-candidate-number-limit 128)
 '(helm-candidate-separator "")
 '(helm-default-external-file-browser "open")
 '(helm-ff-auto-update-initial-value nil)
 '(helm-ff-lynx-style-map nil)
 '(helm-for-files-preferred-list (quote (helm-c-source-ffap-line helm-c-source-ffap-guesser helm-c-source-buffers-list helm-c-source-recentf helm-c-source-bookmarks helm-c-source-file-cache helm-c-source-files-in-current-dir helm-c-source-locate helm-c-source-buffer-not-found)))
 '(helm-home-url "http://www.google.com")
 '(helm-locate-command "locate %s -r %s")
 '(helm-mode t)
 '(helm-mp-matching-method (quote multi3))
 '(helm-quick-update t)
 '(helm-reuse-last-window-split-state nil)
 '(helm-split-window-default-side (quote below))
 '(helm-split-window-in-side-p nil)
 '(helm-time-zone-home-location "Kiev")
 '(helm-top-command "env COLUMNS=%s top -s1 -o cpu -R -F -l1 ")
 '(highlight-indentation-offset 2)
 '(highlight-symbol-idle-delay 1.5)
 '(highlight-symbol-on-navigation-p t)
 '(highline-line nil)
 '(highline-priority 0)
 '(history-delete-duplicates t)
 '(history-length 250)
 '(icicle-mode t)
 '(ido-auto-merge-work-directories-length -1)
 '(ido-create-new-buffer (quote always))
 '(ido-enable-flex-matching nil)
 '(ido-enable-prefix nil)
 '(ido-enable-regexp t)
 '(ido-everywhere t)
 '(ido-hacks-mode t)
 '(ido-mode nil nil (ido))
 '(ido-save-directory-list-file "~/.emacs.d/ido.last")
 '(ido-ubiquitous-function-exceptions (quote (grep-read-files ediff)))
 '(ido-ubiquitous-mode nil)
 '(ido-yes-or-no-mode t)
 '(ielm-mode-hook (quote (turn-on-eldoc-mode)))
 '(imenu-auto-rescan t)
 '(imenu-auto-rescan-maxout 600000)
 '(imenu-sort-function nil)
 '(imenu-tree-auto-update t)
 '(indent-tabs-mode nil)
 '(indicate-buffer-boundaries (quote ((top . left) (bottom . left) (up . left) (down . left))))
 '(inferior-haskell-find-project-root t)
 '(inferior-haskell-wait-and-jump t)
 '(inhibit-startup-echo-area-message "user")
 '(inhibit-startup-screen t)
 '(initial-buffer-choice nil)
 '(initial-frame-alist (quote ((width . 100) (height . 50))))
 '(initial-scratch-message ";;
;;      `~NETSU~'          `-=EMACS=-'
;;
;;
;;        (\"`-'  '-/\") .___..--' ' \"`-._
;;         ` *_ *  )    `-.   (      ) .`-.__. `)
;;         (_Y_.) ' ._   )   `._` ;  `` -. .-'
;;      _.. '--'_..-_/   /-~' _ .' ,4
;;   ( i l ),-''  ( l i),'  ( ( ! .-'
;;
;;

")
 '(jiralib-host "")
 '(jiralib-url "https://menswearhouse.atlassian.net")
 '(js-mode-hook (quote (er/add-js-mode-expansions imenu-add-menubar-index auto-indent-mode-on (lambda nil (hs-org/minor-mode 1)) highline-mode linum-mode (lambda nil (auto-complete-mode 1)) (lambda nil (yas-minor-mode-on)))) t)
 '(jump-char-backward-key "<")
 '(jump-char-forward-key ">")
 '(keyfreq-autosave-mode t)
 '(keyfreq-file "~/.emacs.d/keyfreq")
 '(keyfreq-mode t)
 '(linum+-dynamic-format "  %%%dd")
 '(linum+-smart-format "  %%%dd")
 '(linum-delay nil)
 '(linum-disabled-modes-list (quote (eshell-mode wl-summary-mode compilation-mode org-mode text-mode dired-mode minibuffer-inactive-mode workspace-controller-mode)))
 '(lisp-mode-hook nil)
 '(longlines-wrap-follows-window-size t)
 '(mac-input-method-mode nil)
 '(mail-signature t)
 '(mail-signature-file "~/.emacs.d/wanderlust/signature")
 '(major-mode (quote text-mode))
 '(make-backup-files nil)
 '(max-lisp-eval-depth 10000)
 '(max-specpdl-size 32768)
 '(menu-bar-mode nil)
 '(minimap-always-recenter t)
 '(minimap-hide-fringes t)
 '(minimap-update-delay 0.2)
 '(minimap-width-fraction 0.14)
 '(minimap-window-location (quote right))
 '(mk-proj-ack-cmd "/opt/local/bin/ack-5.12")
 '(mk-proj-use-ido-selection t)
 '(mu-worlds (quote (["\320\241\321\204\320\265\321\200\320\260 \320\234\320\270\321\200\320\276\320\262" "sow.igrohost.ru" 4662 "netsu" "mudLan47ternus"])))
 '(mud-history-max 125)
 '(multi-term-program "/bin/bash")
 '(nxml-child-indent 4)
 '(objc-mode-hook (quote (er/add-cc-mode-expansions (lambda nil (c-toggle-auto-hungry-state -1)) turn-on-auto-fill (lambda nil (progn (make-local-variable (quote auto-indent-assign-indent-level)) (setq auto-indent-assign-indent-level 4))) (lambda nil (auto-indent-mode 1)) linum-mode highline-mode (lambda nil (auto-complete-mode 1)) (lambda nil (ac-cc-mode-setup)) (lambda nil (setq-local ac-sources (append (quote (ac-source-clang ac-source-yasnippet)) ac-sources))) (lambda nil (progn (define-key objc-mode-map (kbd "C-c w") (quote xcdoc:ask-search)) (define-key objc-mode-map (kbd "C-c C-h") (quote c-hungry-delete-backwards)))) (lambda nil (yas-minor-mode-on)) objc-font-lock-mode turn-on-visual-line-mode adaptive-wrap-prefix-mode (lambda nil (setq adaptive-wrap-extra-indent 4)))))
 '(org-completion-use-ido t)
 '(org-default-notes-file "~/Dropbox/Org/tasks.org")
 '(org-directory "~/Dropbox/Org")
 '(org-export-html-style "")
 '(org-export-html-style-extra "
<style type=\"text/css\">
<!--/*--><![CDATA[/*><!--*/

html {
    color: #475b62;
    background-color: #eeeeee;
    font-family: \"Droid Sans\", Helvetica, Verdana, Arial;
}

h1, h2, h3, h4, h5, h6,
h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
    font-family: \"Droid Sans\", Helvetica, Verdana, Arial;
}

#text-table-of-contents a {
    font-family: \"Droid Sans\", Helvetica, Verdana, Arial;
}

h3, h4, h5, h6 {
    font-family: \"Lucida Grande\", Georgia, \"Droid Serif\", \"Times New Roman\", Times;
}

h1 span, h2 span, h3 span, h4 span, h5 span, h6 span {
    font-family: \"Ubuntu Mono\", \"Inconsolata-LGC\", Inconsolata, \"Droid Sans Mono\", Menlo, Consolas, \"Liberation Mono\", \"DejaVu Sans Mono\", Monospace, Monaco, Terminus, \"Courier New\", Courier;
    font-weight: normal;
}

pre {
    border: 1pt solid #A7A6AA;
    background-color: #eeeee0;
    padding: 5pt;
    font-family: \"Ubuntu Mono\", \"Inconsolata-LGC\", Inconsolata, \"Droid Sans Mono\", Menlo, Consolas, \"Liberation Mono\", \"DejaVu Sans Mono\", Monospace, Monaco, Terminus, \"Courier New\", Courier;
    font-size: 90%;
    overflow: auto;
}

body {
    font-size: 14px;
    line-height: 1.5em;
    padding: 0;
    margin: 0;
}
h1 {
    margin: 0;
    font-size: 1.6666666666666667em;
    line-height: 0.9em;
    margin-bottom: 0.9em;
    color: #2E3C3C;
}
h2 {
    margin: 0;
    font-size: 1.5em;
    line-height: 1em;
    margin-bottom: 1em;
}
h3 {
    margin: 0;
    font-size: 1.3333333333333333em;
    line-height: 1.125em;
    margin-bottom: 1.125em;
}
h4 {
    margin: 0;
    font-size: 1.1666666666666667em;
    line-height: 1.2857142857142858em;
    margin-bottom: 1.2857142857142858em;
}
p, ul, blockquote, pre, td, th, label {
    margin: 0;
    font-size: 1em;
    line-height: 1.5em;
    margin-bottom: 1.5em;
}
p.small, #postamble {
    margin: 0;
    font-size: 0.8333333333333334em;
    line-height: 1.8em;
    margin-bottom: 1.8em;
}
table {
    border-collapse: collapse;
    margin-bottom: 1.5em;
}

#content {
    width: 70em;
    margin-left: auto;
    margin-right: auto;
}

#header {
    height: 10em;
}

#table-of-contents {
    color: #475b62;
    width: 15em;
    float: left;
    overflow: auto;
}

div.outline-2 {
        width: 52em;
    float: right;
    position: relative;
}

#postamble {
    color: #7f7f7f;
    clear: both;
    text-align: center;
}

div.outline-2 pre {
    width: 40em;
    overflow: auto;
}

h1.title {
    margin-top: 10px;
    text-align: center;
}

h1.title {
    font-size: 4em;
    font-weight: bold;
    letter-spacing: -0.1em;
    margin-bottom: 0.2em;
}

.todo {
    color: red;
}

.done {
    color: green;
}

.tag {
    color: blue;
    text-transform: lowercase;
    background: #fff;
    border: none;
}

.timestamp {
}

.timestamp-kwd  {

}

.target {

}

#table-of-contents h2 {
    letter-spacing: -0.1em;
}

#table-of-contents ul,
#table-of-contents ol {
    padding-left: 1em;
}

.outline-2 h2 {
    color: #2E3C3C;
    background: #c1cdc1;
    border: none;
}

.outline-2 h2, .outline-2 h3 {
    letter-spacing: -0.05em;
}

.outline-2 {
    padding: 5px;
}

td {
    border: 1px solid #ccc;
}

h1 span, h2 span, h3 span, h4 span, h5 span, h6 span {
    background-color: #E2E1D5;
    padding: 2px;
    border: none;
}

.outline-1, .outline-2, .outline-3, .outline-4, .outline-5, .outline-6 {
    margin-left: 2em;
}

a {
    text-decoration: none;
    color: #57d;
}

a:hover {
    border-bottom: 1px dotted #57d;
}

#postamble p {
    margin: 0px;
}

  /*]]>*/-->
</style>
")
 '(org-export-htmlize-output-type (quote inline-css))
 '(org-export-with-section-numbers nil)
 '(org-hide-leading-stars t)
 '(org-log-done (quote time))
 '(org-modules (quote (org-bbdb org-bibtex org-docview org-gnus org-info org-jsinfo org-irc org-mew org-mhe org-rmail org-vm org-wl org-w3m org-mouse)))
 '(org-replace-disputed-keys t)
 '(org-src-fontify-natively t)
 '(org-todo-keyword-faces (quote (("TODO" . "Red2") ("PROGRESS" . "Orange2") ("SUSPEND" . "Magenta2") ("CANCEL" . "Dark Cyan") ("DONE" . "Dark Green"))))
 '(org-todo-keywords (quote ((sequence "TODO(t)" "PROGRESS(p)" "SUSPEND(s)" "|" "CANCEL(c)" "DONE(d)"))))
 '(package-archives (quote (("gnu" . "http://elpa.gnu.org/packages/") ("ELPA" . "http://tromey.com/elpa/") ("marmalade" . "http://marmalade-repo.org/packages/") ("SC" . "http://joseito.republika.pl/sunrise-commander/"))))
 '(paren-highlight-offscreen t)
 '(paren-message-show-linenumber (quote sexp))
 '(popwin-mode t)
 '(popwin:special-display-config (quote (("*manage-minor-mode*" :regexp nil) ("*yank-pop*" :regexp nil) (speedbar-mode :regexp nil :position left :dedicated t) (direx:direx-mode :regexp nil :width 40 :position left) ("^\\*helm.*\\*$" :regexp t) ("*Kill Ring*") (help-mode) (completion-list-mode :noselect t) (compilation-mode :noselect t) (grep-mode :noselect t) (occur-mode :noselect t) ("*Pp Macroexpand Output*" :noselect t) ("*Shell Command Output*") ("*vc-diff*") ("*vc-change-log*") (" *undo-tree*" :width 60 :position right) ("^\\*anything.*\\*$" :regexp t) ("*slime-apropos*") ("*slime-macroexpansion*") ("*slime-description*") ("*slime-compilation*" :noselect t) ("*slime-xref*") (sldb-mode :stick t) (slime-repl-mode) (slime-connection-list-mode) ("*Buffer List*" :regexp nil) ("*el-get packages*" :regexp nil) ("*Packages*" :regexp nil))))
 '(powerline-default-separator nil)
 '(powerline-text-scale-factor 1.2)
 '(pp^L-^L-string "
")
 '(pp^L-^L-string-pre "")
 '(predictive-main-dict (quote dict-english))
 '(pretty-control-l-mode t)
 '(prj-autotracking nil)
 '(prj-keybindings (quote (([f5] eproject-setup-toggle always) ([s-C-right] eproject-nextfile nil) ([s-C-left] eproject-prevfile nil) ([C-f5] eproject-dired nil))))
 '(prj-set-framepos t)
 '(project-mode t)
 '(project-proj-files-dir "~/.emacs.d/projects")
 '(project-search-exclusion-regexes-default (quote ("~$" "#$" "^\\." "^Builds$" "^Doc$" "\\.git" "\\.xcdatamodeld$" "\\.xib$" "\\.storyboard$" "\\.png$" "\\.gif$" "\\.jpg$" "\\.jpeg$" "\\.tmp$" "\\bTAGS\\b")))
 '(project-tags-form-default (quote (".*" ((quote ignore)))))
 '(projectile-completion-system (quote default))
 '(projectile-enable-caching t)
 '(projectile-global-mode t)
 '(projectile-switch-project-hook (quote (update-projectile-dirtree)))
 '(projectile-tags-command "etags  --declarations -a %s -o %s")
 '(projectile-use-native-indexing t)
 '(recentf-auto-cleanup (quote never))
 '(recentf-exclude (quote ("ido.last" ".ido.last")))
 '(recentf-max-menu-items 48)
 '(recentf-max-saved-items 48)
 '(recentf-mode t)
 '(recentf-save-file "~/.emacs.d/recentf")
 '(regex-tool-backend (quote perl))
 '(revert-without-query (quote (".*.m" ".*.h" ".*.plist" ".*.mm")))
 '(rsense-home "/home/yuriy/.emacs.d/el-get/rsense")
 '(rspec-use-rvm t)
 '(ruby-block-delay 0)
 '(ruby-block-highlight-face (quote highlight-symbol-face))
 '(ruby-block-highlight-toggle t)
 '(rvm-interactive-completion-function (quote helm--completing-read-default))
 '(rvm-interactive-find-file-function (quote helm-find-files))
 '(save-interprogram-paste-before-kill nil)
 '(save-place t nil (saveplace))
 '(save-place-file "~/.emacs.d/emacs-places")
 '(savehist-additional-variables (quote (log-edit-comment-ring)))
 '(savehist-mode t)
 '(sclang-auto-scroll-post-buffer t)
 '(sclang-eval-line-forward nil)
 '(sclang-help-directory "/media/Applications/SuperCollider/Help")
 '(sclang-help-path (quote ("/media/Applications/SuperCollider/Help")))
 '(sclang-mode-hook (quote (turn-on-auto-fill highline-mode (lambda nil (hs-org/minor-mode 1)) highline-mode linum-mode auto-indent-mode-on (lambda nil (auto-complete-mode 1)) (lambda nil (yas-minor-mode-on)))))
 '(sclang-runtime-directory "~/.sclang/")
 '(sclang-show-workspace-on-startup nil)
 '(sclang-source-directory "~/.sclang/")
 '(scroll-bar-mode nil)
 '(scroll-margin 0)
 '(semanticdb-default-save-directory "~/.emacs.d/semanticdb/")
 '(server-mode t)
 '(sgml-mode-hook (quote (zencoding-mode)))
 '(shell-file-name "/bin/bash")
 '(shell-switcher-mode t)
 '(shift-select-mode nil)
 '(show-paren-mode t)
 '(show-smartparens-global-mode t)
 '(show-trailing-whitespace nil)
 '(size-indication-mode t)
 '(smartparens-global-mode t)
 '(smex-flex-matching nil)
 '(smex-history-length 512)
 '(smex-save-file "~/.emacs.d/smex-items")
 '(sml/col-number-format "%2c")
 '(sml/hidden-modes (quote (" hl-p" " mate" " WLR" " UT" " VHl" " hl" " Helm" " Anzu" " Projectile")))
 '(sml/line-number-format "%2l")
 '(sml/modified-char " X ")
 '(sml/name-width 44)
 '(sml/override-theme nil)
 '(sml/position-percentage-format " %p")
 '(sml/replacer-regexp-list (quote ((":DB:Org/" ":Org:") ("^~/Drive/pitometsu@gmail.com/" ":Drive:") ("^~/Projects/" ":Proj:") ("^~/\\.emacs\\.d/" ":ED:") ("^/sudo:.*:" ":SU:") ("^~/Documents/" ":Doc:") ("^~/Dropbox/" ":DB:") ("^:\\([^:]*\\):Documento?s/" ":\\1/Doc:") ("^~/[Gg]it/" ":Git:") ("^~/[Gg]it[Hh]ub/" ":Git:") ("^~/[Gg]it-?[Pp]rojects/" ":Git:"))))
 '(sml/shorten-modes t)
 '(sml/show-client nil)
 '(sml/show-eol nil)
 '(sml/show-file-name nil)
 '(smooth-scroll-margin 6)
 '(smooth-scroll-strict-margins t)
 '(sp-base-key-bindings nil)
 '(sp-wrap-repeat-last 2)
 '(speedbar-fetch-etags-arguments (quote ("-d" "--declarations" "--globals" "--members" "-t" "-T" "--regex='/^[ \\n\\t]*#pragma^[ \\n\\t]*mark/'" "-I" "-o" "-")))
 '(speedbar-tag-hierarchy-method (quote (speedbar-prefix-group-tag-hierarchy)))
 '(speedbar-use-images nil)
 '(sr-speedbar-max-width 32)
 '(sr-speedbar-right-side nil)
 '(sr-speedbar-width-console 32)
 '(sr-speedbar-width-x 32)
 '(sr-terminal-program "multi-term")
 '(tab-always-indent (quote complete))
 '(tab-width 4)
 '(temp-buffer-resize-mode t)
 '(term-buffer-maximum-size 10000)
 '(tool-bar-mode nil)
 '(trash-directory "~/.Trash/emacs")
 '(undo-tree-enable-undo-in-region nil)
 '(undo-tree-mode-lighter " UT")
 '(undo-tree-visualizer-diff t)
 '(undo-tree-visualizer-timestamps t)
 '(unicode-whitespace-newline-mark-modes (quote (indented-text-mode makefile-automake-mode makefile-bsdmake-mode makefile-gmake-mode makefile-imake-mode makefile-makepp-mode makefile-mode markdown-mode org-mode snippet-mode text-mode emacs-lisp-mode ruby-mode)))
 '(uniquify-buffer-name-style (quote post-forward) nil (uniquify))
 '(user-full-name "Pitomets Yuriy")
 '(user-mail-address "pitometsu@gmail.com")
 '(visible-bell t)
 '(w3m-charset-coding-system-alist (quote ((utf-8 . undecided) (x-sjis . shift_jis) (x-shift_jis . shift_jis) (x-shift-jis . shift_jis) (x-euc-jp . euc-japan) (shift-jis . shift_jis) (x-unknown . undecided) (unknown . undecided) (windows-874 . tis-620) (us_ascii . raw-text) (koi8-r . undecided) (windows-1251 . undecided) (windows-1252 . undecided))))
 '(w3m-coding-system (quote utf-8))
 '(w3m-command nil)
 '(w3m-default-coding-system (quote utf-8))
 '(w3m-default-display-inline-images t)
 '(w3m-default-save-directory "~/Downloads/w3m")
 '(w3m-file-coding-system (quote utf-8))
 '(w3m-file-name-coding-system (quote utf-8))
 '(w3m-home-page "google.com")
 '(w3m-init-file "~/.emacs.d/emacs-w3m")
 '(w3m-key-binding (quote info))
 '(w3m-new-session-url "google.com")
 '(w3m-pop-up-windows nil)
 '(w3m-session-load-crashed-sessions t)
 '(w3m-session-load-last-sessions t)
 '(w3m-terminal-coding-system (quote utf-8))
 '(w3m-toggle-inline-images-permanently nil)
 '(w3m-use-cookies t)
 '(w3m-use-header-line nil)
 '(w3m-use-tab nil)
 '(w3m-use-tab-menubar t)
 '(w3m-use-toolbar t)
 '(warning-minimum-level :error)
 '(warning-minimum-log-level :error)
 '(wg-default-session-file "~/.emacs.d/emacs_workgroups")
 '(wget-download-directory "~/Downloads")
 '(which-func-format (quote ((:propertize which-func-current local-map (keymap (mode-line keymap (mouse-3 . end-of-defun) (mouse-2 . narrow-to-defun) (mouse-1 . beginning-of-defun))) face which-func mouse-face mode-line-highlight help-echo "mouse-1: go to beginning
mouse-2: toggle rest visibility
mouse-3: go to end"))))
 '(which-func-modes (quote (c-mode c++-mode perl-mode cperl-mode python-mode makefile-mode sh-mode fortran-mode f90-mode ada-mode diff-mode objc-mode emacs-lisp-mode haskell-mode sclang-mode ruby-mode)))
 '(which-function-mode t)
 '(whitespace-display-mappings (quote ((space-mark 32 [183] [46]) (space-mark 160 [164]) (newline-mark 10 [172 10]) (tab-mark 9 [183 9] [164 9] [92 9]))))
 '(whitespace-line-column 80)
 '(whitespace-style (quote (face spaces indentation tabs newline space-before-tab space-after-tab indentation space-mark tab-mark newline-mark)))
 '(whole-line-or-region-extensions-alist (quote ((copy-region-as-kill whole-line-or-region-copy-region-as-kill nil) (kill-region whole-line-or-region-kill-region nil) (kill-ring-save whole-line-or-region-kill-ring-save nil) (yank whole-line-or-region-yank nil) (comment-or-uncomment-region whole-line-or-region-comment-dwim-2 nil) (ns-copy-including-secondary whole-line-or-region-kill-ring-save nil))))
 '(windmove-wrap-around t)
 '(winner-mode t nil (winner))
 '(wl-address-file "~/.emacs.d/wanderlust/addresses")
 '(wl-alias-file "~/.emacs.d/wanderlust/im/Aliases")
 '(wl-default-folder "%Inbox")
 '(wl-draft-folder "%[Gmail]/Draft")
 '(wl-folder-window-width 25)
 '(wl-folders-file "~/.emacs.d/wanderlust/folders")
 '(wl-from "")
 '(wl-init-file "~/.emacs.d/wanderlust/wl")
 '(wl-local-domain "gmail.com")
 '(wl-message-ignored-field-list (quote ("^.*")))
 '(wl-message-visible-field-list (quote ("^From:" "^Organization:" "^To:" "^Cc:" "^Date:" "^Subject:" "^User-Agent:" "^X-Mailer:")))
 '(wl-organization "Netsu")
 '(wl-smtp-authenticate-type "plain")
 '(wl-smtp-connection-type (quote starttls))
 '(wl-smtp-posting-port 587)
 '(wl-smtp-posting-server "smtp.gmail.com")
 '(wl-smtp-posting-user "Pitometsu")
 '(wl-subscribed-mailing-list (quote ("ypitomets@provectus-it.com")))
 '(wl-summary-line-format "%n%T%P %W %D-%M-%Y %h:%m %t%[%c %f% %] %s")
 '(wl-summary-weekday-name-lang "ru")
 '(wl-summary-width 80)
 '(wl-trash-folder "%[Gmail]/Trash")
 '(workgroups-mode t)
 '(yas-global-mode nil nil (yasnippet)))


;; Advanced flags

;; open subdir in same buffer
(put 'dired-find-alternate-file 'disabled nil)

;; downcase whole region
(put 'downcase-region 'disabled nil)

;; horisontal scroll
(put 'scroll-left 'disabled nil)

;; upcase whole region
(put 'upcase-region 'disabled nil)

;; folding the rest of code
(put 'narrow-to-defun  'disabled nil)
(put 'narrow-to-page   'disabled nil)
(put 'narrow-to-region 'disabled nil)


;; Load libs

;; subWords
(load "subword")

;; unique buffer names
(require 'uniquify)

;; dired-x
(require 'dired-x)

;; gcopy-from-above-command
(require 'misc)

;; second virtual cursor
(require 'vcursor)

;; keybindings
(load "keybindings")

;; customizing
(load "customizing")

;; hooks
(load "hooks")

;; All done
(load "done")
