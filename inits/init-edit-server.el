;; Integrate Emacs with Stack Exchange http://stackoverflow.com/a/10386560/789593
(add-to-list 'auto-mode-alist
			 '("stack\\(exchange\\|overflow\\)\\.com\\.[a-z0-9]+\\.txt"
			   . markdown-mode))

(edit-server-start)
