(require 'window-number)
(window-number-mode)
(window-number-meta-mode)


(autoload 'window-number-meta-mode "window-number"
  "A global minor mode that enables use of the M- prefix to select
windows, use `window-number-mode' to display the window numbers in
the mode-line."
  t)

;; ----------------------------------------------------------------------------
(window-number-meta-mode t)

(define-key window-number-mode-map (kbd "C-x C-j") 'dired-jump)
