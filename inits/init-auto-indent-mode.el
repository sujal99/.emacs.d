(require 'auto-indent-mode)

(autoload 'auto-indent-yank "auto-indent-mode" "" t)
(autoload 'auto-indent-yank-pop "auto-indent-mode" "" t)

(define-key global-map [remap yank] 'auto-indent-yank)
(define-key global-map [remap yank-pop] 'auto-indent-yank-pop)

(autoload 'auto-indent-delete-char "auto-indent-mode" "" t)
(define-key global-map [remap delete-char] 'auto-indent-delete-char)

(autoload 'auto-indent-kill-line "auto-indent-mode" "" t)
(define-key global-map [remap kill-line] 'auto-indent-kill-line)
