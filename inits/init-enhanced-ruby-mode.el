(add-to-list 'auto-mode-alist '("\\Gemfile$" . enh-ruby-mode))
(add-to-list 'auto-mode-alist '("\\Rakefile$" . enh-ruby-mode))
(add-to-list 'auto-mode-alist '("\\.rake$" . enh-ruby-mode))
(add-to-list 'auto-mode-alist '("\\.gemspec$" . enh-ruby-mode))
(add-to-list 'auto-mode-alist '("\\.thor$" . enh-ruby-mode))
(add-to-list 'auto-mode-alist '("\\Podfile$" . enh-ruby-mode))
(add-to-list 'auto-mode-alist '("\\.podspec$" . enh-ruby-mode))

(define-key enh-ruby-mode-map (kbd "C-c {") 'ruby-toggle-block)
