(require 'pp-c-l)
;; make the formfeed char display as a line
;; 2011-07-14 commented out due to a display problem with whitespace-mode
;; http://groups.google.com/group/gnu.emacs.help/browse_frm/thread/12e5a1e6a8b22c14/c642875edeb7ea20
(pretty-control-l-mode 1) ;; it has conflicts with “whitespace-mode” settings
