(require 'markdown-mode)
(setq auto-mode-alist
      (append auto-mode-alist
              '((" \\ .text$"    . markdown-mode)
                (" \\ .md$"      . markdown-mode)
                (" \\ .textile$" . markdown-mode)
                (" \\ .mkdn$"    . markdown-mode)
                (" \\ .mdwn$"    . markdown-mode)
                (" \\ .mdt$"     . markdown-mode))))
