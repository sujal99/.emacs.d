(require 'rdoc-mode)
(add-to-list 'auto-mode-alist '("\\.rdoc$" . rdoc-mode))
