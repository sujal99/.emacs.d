(require 'whole-line-or-region)
;; fix respect the `kill-read-only-ok' variable
(defadvice whole-line-or-region-kill-region
  (before whole-line-or-region-kill-read-only-ok activate)
  (interactive "p")
  (unless kill-read-only-ok (barf-if-buffer-read-only)))
