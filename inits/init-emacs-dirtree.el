(defun projectile-dirtree (&optional select)
  "Open `dirtree' with `projectile-project-root'.
With prefix arguement select `current-buffer'."
  (interactive "P")
  (let ((win (get-buffer-window (current-buffer)))
		(dirtree-old-buffer (get-buffer dirtree-buffer)))
	(when dirtree-old-buffer
	  (kill-buffer dirtree-old-buffer))
	(dirtree (projectile-project-root) nil)
	(when select
	  (select-window win))))

(defun update-projectile-dirtree ()
  "Just show `projectile-dirtree' if `dirtree' buffer already present."
  (let ((dirtree-old-buffer (get-buffer dirtree-buffer)))
    (when dirtree-old-buffer
	  (projectile-dirtree t))))
