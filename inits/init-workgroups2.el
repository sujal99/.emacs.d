(global-unset-key (kbd "C-<tab>"))
(global-set-key (kbd "C-<tab>") 'wg-switch-to-workgroup)
(global-set-key (kbd "C-S-<tab>") 'wg-switch-to-previous-workgroup)

;; fix org-mode bindings
(define-key org-mode-map (kbd "C-<tab>") 'wg-switch-to-workgroup)
