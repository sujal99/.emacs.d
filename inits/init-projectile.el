(require 'projectile)
(projectile-global-mode)
(after-mode 'helm-projectile
  (global-set-key (kbd "C-c h") 'helm-projectile)
  (global-set-key (kbd "s-h") 'helm-projectile))
