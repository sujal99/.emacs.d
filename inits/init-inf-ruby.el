(defun inf-ruby-console-rake ()
  "Run IRB rake console."
  (interactive)
  (run-ruby "bundle exec rake console"))
