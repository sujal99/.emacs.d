(require 'col-highlight)
(setq col-highlight-period 0.6)
(global-set-key (kbd "C-+") 'column-highlight-mode)
(global-set-key (kbd "C-=") 'flash-column-highlight)
