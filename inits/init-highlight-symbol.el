(require 'highlight-symbol)
(global-set-key (kbd "C-*") 'highlight-symbol-next)
(global-set-key (kbd "C-#") 'highlight-symbol-prev)
(global-set-key (kbd "C-)") 'highlight-symbol-next)
(global-set-key (kbd "C-(") 'highlight-symbol-prev)
(highlight-symbol-mode) ;; enable global highlighting symbol under cursor
(global-set-key (kbd "M-s-e") 'highlight-symbol-query-replace)
