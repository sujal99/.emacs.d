(require 'yank-pop-summary)
(global-set-key "\M-y" 'yank-pop-forward)
(global-set-key "\C-\M-y" 'yank-pop-backward)

(global-set-key (kbd "s-M-V") 'yank-pop-forward)
(global-set-key (kbd "s-S-V") 'yank-pop-backward)
(after-mode 'helm-ring
  (global-set-key (kbd "s-C-M-V") 'helm-show-kill-ring))
