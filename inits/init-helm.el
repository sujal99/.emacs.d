(require 'helm)
(require 'helm-config)

(global-set-key (kbd "M-o") 'helm-occur)
(global-set-key (kbd "C-M-o") 'helm-multi-occur)

(global-set-key (kbd "s-O") 'helm-multi-occur)
(global-set-key (kbd "s-o") 'helm-occur)

;; like sublimetext's Ctrl-P file@symbol
(defun helm-goto-symbol ()
  "multi-occur in all buffers backed by files."
  (interactive)
  (helm-multi-occur
   (delq nil
         (mapcar (lambda (b)
                   (when (buffer-file-name b) (buffer-name b)))
				 (buffer-list)))))

;; keybindings for Helm
(global-set-key (kbd "M-x")     'helm-M-x)
(global-set-key (kbd "C-x C-R") 'helm-recentf)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "s-p")     'helm-goto-symbol)
(global-set-key (kbd "C-x p")   'helm-goto-symbol)
(global-set-key (kbd "C-x b")   'helm-buffers-list)
(global-set-key (kbd "C-x s-F") 'helm-do-grep)
(global-set-key (kbd "s-F")     'helm-do-grep)

;; enable helm pcomplete
(add-hook 'eshell-mode-hook
          #'(lambda ()
              (define-key eshell-mode-map
                [remap eshell-pcomplete]
                'helm-esh-pcomplete)))

;; enable helm Eshell history
(add-hook 'eshell-mode-hook
		  #'(lambda ()
			  (define-key eshell-mode-map
				(kbd "M-p")
				'helm-eshell-history)))

(define-key helm-map (kbd "C-g") 'helm-keyboard-quit)
