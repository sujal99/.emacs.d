(setq direx:leaf-icon "  "
	  direx:open-icon "▼ "
	  direx:closed-icon "▶ ")

(global-set-key (kbd "s-M-p") 'direx-project:jump-to-project-root-other-window)
