(require 'rspec-mode)

(after-mode 'yasnippet
  (eval-after-load 'rspec-mode
	'(rspec-install-snippets)))

;; fix zsh vs rvm compatibility
(defadvice rspec-compile (around rspec-compile-around)
  "Use BASH shell for running the specs because of ZSH issues."
  (let ((shell-file-name "/bin/bash"))
    ad-do-it))
(ad-activate 'rspec-compile)
