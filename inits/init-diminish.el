(require 'diminish)

(after-mode 'projectile
  (diminish 'projectile-mode "Proj"))
(after-mode 'yasnippet
  (diminish 'yas-minor-mode "Snip"))
(after-mode 'rspec-mode
  (diminish 'rspec-mode "Spec"))
(after-mode 'simple
  (diminish 'auto-fill-function))
(after-mode 'helm-mode
  (diminish 'helm-mode))
(after-mode 'highlight-80+
  (diminish 'highlight-80+-mode))
(after-mode 'flymake
  (diminish 'flymake-mode))
(after-mode 'ruby-tools
  (diminish 'ruby-tools-mode))
(after-mode 'anzu
  (diminish 'anzu-mode))
(after-mode 'highlight-indentation
  (diminish 'highlight-indentation-current-column-mode))
(after-mode 'highlight-indentation
  (diminish 'highlight-indentation-mode))
(after-mode 'highline
  (diminish 'highline-mode))
(after-mode 'volatile-highlights
  (diminish 'volatile-highlights-mode))
(after-mode 'ruby-block
  (diminish 'ruby-block-mode))
(after-mode 'whole-line-or-region
  (diminish 'whole-line-or-region-mode))
(after-mode 'textmate
  (diminish 'textmate-mode))
(after-mode 'undo-tree
  (diminish 'undo-tree-mode))
(after-mode 'smartparens
  (diminish 'smartparens-mode))
(after-mode 'auto-indent-mode
  (diminish 'auto-indent-mode))
(after-mode 'checkdoc
  (diminish 'checkdoc-minor-mode))
(after-mode 'eldoc
  (diminish 'eldoc-mode))
(after-mode 'abbrev
  (diminish 'abbrev-mode))
(after-mode 'window-number
  (diminish 'window-number-mode))
(after-mode 'yard-mode
  (diminish 'yard-mode))
(after-mode 'robe
  (diminish 'robe-mode))
(after-mode 'ruby-refactor
  (diminish 'ruby-refactor-mode))
;; (after-mode 'workgroups2
;;   (diminish 'workgroups-mode))
(defadvice workgroups-mode (after workgroups-mode-diminish-advice activate compile)
  (after-mode 'workgroups2
    (diminish 'workgroups-mode)))
(after-mode 'guide-key
  (diminish 'guide-key-mode))
(after-mode 'ruby-end
  (diminish 'ruby-end-mode))
(after-mode 'git-gutter
  (diminish 'git-gutter-mode))
(after-mode 'drag-stuff
  (diminish 'drag-stuff-mode))
