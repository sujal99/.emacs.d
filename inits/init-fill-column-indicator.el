(require 'fill-column-indicator)
(defadvice text-scale-adjust (after re-enable-fci-mode activate)
  (while fci-mode
	(turn-off-fci-mode)
	(turn-on-fci-mode)))
