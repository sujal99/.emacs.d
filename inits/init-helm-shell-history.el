(require 'helm-shell-history)
(add-hook 'term-mode-hook
		  (lambda ()
			(define-key term-raw-map (kbd "C-r") 'helm-shell-history)))
