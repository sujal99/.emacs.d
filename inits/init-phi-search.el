(require 'phi-search)

(after-mode 'init-multiple-cursors
  (define-key mc-mode-map (kbd "C-s") 'phi-search)
  (define-key mc-mode-map (kbd "s") 'phi-search))
