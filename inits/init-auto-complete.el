(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)

(ac-linum-workaround)
(ac-flyspell-workaround)
(define-key ac-completing-map (kbd "C-n") 'ac-next)
(define-key ac-completing-map (kbd "C-p") 'ac-previous)
(define-key ac-completing-map (kbd "M-/") 'ac-stop)
(global-set-key (kbd "M-?") 'ac-start)
(global-set-key (kbd "\t") 'ac-complete)

;; sources
(setq-default ac-sources (append '(
                                   ;; ac-source-abbrev
                                   ;; ac-source-dictionary
                                   ac-source-imenu
                                   ac-source-yasnippet
                                   ;; ac-source-words-in-same-mode-buffers
                                   ) ac-sources))
