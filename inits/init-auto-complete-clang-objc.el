(require 'auto-complete-config)
(require 'auto-complete-clang-objc)

;; TODO: flymake
;; TODO: `cc-search-directories'
;; TODO: `auto-complete-c-headers'

;; (setq cc-other-file-alist
;;       `(("\\.cpp$" (".hpp" ".h"))
;;         ("\\.h$" (".c" ".cpp" ".m" ".mm"))
;;         ("\\.hpp$" (".cpp" ".c"))
;;         ("\\.m$" (".h"))
;;         ("\\.mm$" (".h"))
;;         ))
;; (add-hook 'c-mode-common-hook (lambda() (local-set-key (kbd "C-c o") 'ff-find-other-file)))

;; env xcrun xcodebuild
;; (shell-command-to-string "env xcrun xcodebuild -showBuildSettings 2>
;; /dev/null")
;; -list
;; -workspace
;; -scheme
;; -configuration
;; -project
;; -target
;; -alltargets


(defvar xcode:iphone-deployment-target "6.1"
  "env xcrun xcodebuild -showBuildSettings 2> /dev/null | grep IPHONEOS_DEPLOYMENT_TARGET")
(defvar xcode:sdk-dir
  "/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator7.0.sdk"
  "env xcrun xcodebuild -showBuildSettings 2> /dev/null | grep SDK_DIR")
(defvar xcode:source-root "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football"
  "env xcrun xcodebuild -showBuildSettings 2> /dev/null | grep SOURCE_ROOT")
(defvar xcode:header-search-path
    '("/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/A2StoryboardSegueContext" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/AFHTTPRequestOperationLogger" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/AFNetworking" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/AFOAuth2Client" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/FMDB" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/Facebook-iOS-SDK" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/Facebook-iOS-SDK/FacebookSDK" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/FormatterKit" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/GCJSONKit" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/GCJSONKit/JSONKit" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/IAPManager" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/ISO8601DateFormatter" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/NSData+Base64" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/RATreeView" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/RHAddressBook" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/Reachability" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/SBData" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/SecureUDID" "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/sqlite3")
    "env xcrun xcodebuild -showBuildSettings 2> /dev/null | grep HEADER_SEARCH_PATHS")
(defvar xcode:other-flags
    '("-DNS_BLOCK_ASSERTIONS=1" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/A2StoryboardSegueContext" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/AFHTTPRequestOperationLogger" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/AFNetworking" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/AFOAuth2Client" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/FMDB" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/Facebook-iOS-SDK" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/Facebook-iOS-SDK/FacebookSDK" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/FormatterKit" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/GCJSONKit" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/GCJSONKit/JSONKit" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/IAPManager" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/ISO8601DateFormatter" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/NSData+Base64" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/RATreeView" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/RHAddressBook" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/Reachability" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/SBData" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/SecureUDID" "-isystem/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Pods/Headers/sqlite3")
    "env xcrun xcodebuild -showBuildSettings 2> /dev/null | grep OTHER_CFLAGS")
(defvar xcode:framework-search-path
  "/Users/netsu/Projects/fantasysports/fantasysports/ios/FMF Football/Vendor/UbertestersSDK"
  "env xcrun xcodebuild -showBuildSettings 2> /dev/null | grep FRAMEWORK_SEARCH_PATHS")
(defvar xcode:gcc-prefix-header "App/FMF Football-Prefix.pch"
  "env xcrun xcodebuild -showBuildSettings 2> /dev/null | grep GCC_PREFIX_HEADER")



;; Xcode stuff

(require 'f)

(defgroup xcode:auto-complete nil
  "Clang `auto-complete' intergation with Xcode projects."
  :group 'auto-complete)

(defsubst xcode:ios-version ()
  "Return Clang constant name for current iOS version.

See `xcode:iphone-deployment-target' also."
  (format "__IPHONE_%s" (replace-regexp-in-string "\\." "_" xcode:iphone-deployment-target)))

(defun xcode:expand-file-name (FILE)
  "Return quoted expanded name of FILE."
  (shell-quote-argument
   (expand-file-name FILE)))

(defcustom xcode:project-headers-directory nil
  "Directory path for searching headers.

For example: \"Classes\", \"Headers\", \"App\".
If empty, `xcode:source-root' will be used."
  :type 'file
  :group 'xcode:auto-complete)

(defsubst xcode:project-headers-directory ()
    "Return directory that should contain project's headers."
    (if xcode:project-headers-directory
            xcode:project-headers-directory
        xcode:source-root))

(defsubst xcode:compile-default-options ()
  "Clang default options for autocompletion."
  (let ((xcode:sdk-frameworks-path (xcode:expand-file-name (concat xcode:sdk-dir "/System/Library/Frameworks/")))
        (xcode:sdk-include-path (xcode:expand-file-name (concat xcode:sdk-dir "/usr/include/"))))
    (list
     "-std=c99"
     "-w"
     "-fblocks"
     "-fobjc-arc"
     (format "-D__IPHONE_OS_VERSION_MIN_REQUIRED=%s" (xcode:ios-version))
     (format "-isysroot %s" xcode:sdk-dir)
     "-O0"
     "-fno-rtti"
     "-fvisibility-inlines-hidden"
     "-fno-threadsafe-statics"
     "-fsyntax-only"
     (format "-F%s" xcode:sdk-frameworks-path)
     (format "-isystem%s" xcode:sdk-include-path))))

;; auto-complete-clang
(setq-default ac-clang-flags (xcode:compile-default-options))



;; FIXes

(defun ac-clang-candidate ()
  (unless (ac-in-string/comment)
    (and ac-clang-auto-save
         (buffer-modified-p)
         (basic-save-buffer))
    (save-restriction
      (widen)
      (ac-clang-call-process
       ac-prefix
       (ac-clang-build-complete-args (- (point) (length ac-prefix)))))))

(defun ac-clang-handle-error (res call-options)
  (goto-char (point-min))
  (let* ((buf (get-buffer-create ac-clang-error-buffer-name))
         (cmd (format "/bin/bash -c '%s'" call-options))
         (pattern (format ac-clang-completion-pattern ""))
         (err (if (re-search-forward pattern nil t)
                  (buffer-substring-no-properties (point-min)
                                                  (1- (match-beginning 0)))
                ;; Warn the user more agressively if no match was found.
                (message "clang failed with error %d:\n%s" res cmd)
                (buffer-string))))

    (with-current-buffer buf
      (let ((inhibit-read-only t))
        (erase-buffer)
        (insert (current-time-string)
                (format "\nclang failed with error %d:\n" res)
                cmd "\n\n")
        (insert err)
        (setq buffer-read-only t)
        (goto-char (point-min))))))

(defun ac-clang-call-process (prefix args)
  (let* ((buf (get-buffer-create "*clang-output*"))
         (call-options (format "%s" (mapconcat 'identity args " ")))
         res)
    (with-current-buffer buf (erase-buffer))
    (let ((res (if ac-clang-auto-save
                   (call-process "/bin/bash" nil buf nil "-c" call-options)
                 (call-process-region (point-min) (point-max) "/bin/bash" nil buf nil "-c" call-options))))
      (with-current-buffer buf
        (unless (eq 0 res)
          (ac-clang-handle-error res call-options))
        ;; Still try to get any useful input.
        (ac-clang-parse-output prefix)))))

(defsubst ac-clang-build-location (pos)
  (save-excursion
    (goto-char pos)
    (format "%s:%d:%d" (if ac-clang-auto-save
                           (xcode:expand-file-name buffer-file-name)
                         "-")
            (line-number-at-pos)
            (1+ (- (point) (line-beginning-position))))))

(defsubst ac-clang-build-complete-args (pos)
  (append
   (list
    "/usr/bin/env"
    "xcrun"
    "clang"
    "-cc1")
   (unless ac-clang-auto-save
     (list "-x" (ac-clang-lang-option)))
   (list
    (format"-code-completion-at %s %s"
           (ac-clang-build-location pos)
           (if ac-clang-auto-save (xcode:expand-file-name buffer-file-name) "-")))
   ac-clang-flags
   (list
    (format "-include %s"
            (shell-quote-argument (format "%s/%s" xcode:source-root xcode:gcc-prefix-header))))
   (mapcar (lambda (path) (format "-I%s" (shell-quote-argument path)))
           xcode:header-search-path)
   (mapcar (lambda (path) (format "-I%s" (shell-quote-argument path)))
           (f-directories (xcode:project-headers-directory) nil t))
   (mapcar (lambda (path) (shell-quote-argument path)) xcode:other-flags)
   (list
    (format "-F%s" (shell-quote-argument xcode:framework-search-path)))))
