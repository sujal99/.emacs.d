(global-unset-key (kbd "M-j"))
(global-set-key (kbd "M-j") 'jump-char-forward)
(global-set-key (kbd "M-J") 'jump-char-backward)

;;fix ace-jump-mode integration
(after-mode 'ace-jump-mode
  (define-key jump-char-isearch-map (kbd "C-c C-c") 'jump-char-switch-to-ace)
  (define-key jump-char-isearch-map (kbd "M-/") 'jump-char-switch-to-ace))
