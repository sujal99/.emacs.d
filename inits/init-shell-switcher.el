(define-key shell-switcher-mode-map (kbd "C-c '")  'shell-switcher-switch-buffer)
(define-key shell-switcher-mode-map (kbd "C-c \"") 'shell-switcher-switch-buffer-other-window)
(define-key shell-switcher-mode-map (kbd "C-'")    'nil)
(define-key shell-switcher-mode-map (kbd "C-M-'")  'nil)
