# The EMACS configuration directory
***
## configured for
 * Ruby hacking
 * Ruby on Rails development
 * Clang Objective-C iOS development
 * SuperCollider algorithmic audiosynthesis
 * Haskell hacking
***
 ![Screenshot](http://i.imgur.com/0IvEYcP.png)

## References
### coding fonts
- [Inconsolata](http://levien.com/type/myfonts/inconsolata.html)
- [Ubuntu Mono](http://font.ubuntu.com)
- [Droid Mono Slashed](http://blog.cosmix.org/2009/10/27/a-slashed-zero-droid-sans-mono/)
- [Droid Mono Dotted](http://blog.cosmix.org/2009/01/25/a-dotted-zero-droid-sans-mono/)
- [MigMix](http://mix-mplus-ipa.sourceforge.jp/migmix/)
